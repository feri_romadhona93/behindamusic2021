<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Soundrenaline
use App\Http\Controllers\SoundreController;
Route::get('/soundrenaline', [SoundreController::class,'homepage']);
Route::get('/soundrenaline/creators-park', [SoundreController::class,'creatorsPark']);
Route::get('/soundrenaline/performance', [SoundreController::class,'performance']);
Route::get('/soundrenaline/performance/{id}/{titleSlug}', [SoundreController::class,'performance']);
Route::get('/soundrenaline/contact-us', [SoundreController::class,'contactUs']);
Route::get('/soundrenaline/schedule', [SoundreController::class,'schedule']);
//Route::get('/soundrenaline/direct', [SoundreController::class,'bypassAgeVerification']);
//Route::get('/soundrenaline/branded', [SoundreController::class,'bypassUnbranded']);
Route::post('/soundrenaline/verify-age/submit', [SoundreController::class,'verifyAgeSubmit']);
Route::post('/soundrenaline/enter/event', [SoundreController::class,'enterEvent']);

//Route::get('/soundrenaline/privy/submission-info', [SoundreController::class,'privySubmissionInfo']);
//Route::get('/soundrenaline/test-verify', [SoundreController::class,'testVerify']);
//Route::get('/soundrenaline/test-logout', [SoundreController::class,'testLogout']);
//Route::get('/soundrenaline/test-data', [SoundreController::class,'testData']);
//Route::get('/soundrenaline/test-data/{id}', [SoundreController::class,'testData']);
//Route::post('/soundrenaline/test-verify/submit', [SoundreController::class,'testVerifySubmit']);
