<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class RedirectToNonWWW
{
    public function handle(Request $request, Closure $next)
    {
        if (substr($request->header('host'), 0, 4) == 'www.') {
            $request->headers->set('host', 'rctiplus.com');
            return Redirect::to($request->path());
        }
        return $next($request);
    }
}
