<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class SoundreDataController extends Controller
{
    function getConfig(): ?\stdClass
    {
        $cfg =  DB::select(
            "SELECT * FROM configs LIMIT 1"
        );
        if ($cfg) {
            return $cfg[0];
        }
        return null;
    }

    function getPageMetaByType(string $type): ?\stdClass {
        $staticEndpoint = config('app.static_endpoint');
        $meta = DB::select(
            "
                SELECT pm.*, CONCAT('".$staticEndpoint."', uf.url) AS meta_image_url
                FROM page_metas AS pm
                LEFT JOIN upload_file_morph AS ufm ON ufm.related_type = 'page_metas' AND ufm.related_id = pm.id
                LEFT JOIN upload_file AS uf ON uf.id = ufm.upload_file_id
                WHERE `type` = '".$type."'
            "
        );
        if ($meta) {
            return $meta[0];
        }
        return null;
    }

    function getAllHomepageBanners(): ?array
    {
        $staticEndpoint = config('app.static_endpoint');
        $homepageBanners =  DB::select(
            "
                SELECT h.*, CONCAT('".$staticEndpoint."', uf.url) AS thumbnail_url FROM homepages AS h
                LEFT JOIN upload_file_morph AS ufm ON ufm.related_type = 'homepages' AND ufm.related_id = h.id
                LEFT JOIN upload_file AS uf ON uf.id = ufm.upload_file_id ORDER BY h.id ASC
            "
        );
        if (count($homepageBanners) > 0) {
         return $homepageBanners;
        }
        return null;
    }

    function getAllWeeksFromPerformances(): ?array {
        $allWeeksGroup = [];
        $allWeeks =  DB::select(
            "SELECT p.week AS week
                FROM performances AS p
                WHERE p.active_status = 1 AND (p.creators_park_exclusive != 1 OR p.creators_park_exclusive IS NULL)
                GROUP BY p.week
                ORDER BY p.week ASC
            "
        );
        foreach ($allWeeks as $key => $val) {
            $allWeeksGroup[$key] = $val->week;
        }
        if (count($allWeeksGroup) > 0) {
            return $allWeeksGroup;
        }
        return null;
    }

    function getCurrentPlayedPerformanceById(int $id): ?\stdClass {
        $currentDate = $this->getCurrentDate();
        $staticEndpoint = config('app.static_endpoint');
        $performance = DB::select(
            "
                SELECT
                    p.*,
                    CONCAT('".$staticEndpoint."', uf.url) AS thumbnail_url,
                    CASE
                        WHEN ('".$currentDate."' >= p.start_live AND '".$currentDate."' <= p.end_live) THEN 'live'
                        WHEN ('".$currentDate."' < p.start_live) THEN 'coming_soon'
                        WHEN ('".$currentDate."' > p.expired_date) THEN 'expired'
                        ELSE 'vod'
                    END AS performance_status_now
                FROM performances AS p
                LEFT JOIN upload_file_morph AS ufm ON ufm.related_type = 'performances' AND ufm.related_id = p.id
                LEFT JOIN upload_file AS uf ON uf.id = ufm.upload_file_id
                WHERE p.id = ? AND p.active_status = 1 AND (p.creators_park_exclusive != 1 OR p.creators_park_exclusive IS NULL)
                ORDER BY p.start_live ASC
                LIMIT 1
            ", [$id]
        );
        if ($performance) {
            $performance[0]->title_slug = Str::slug($performance[0]->title, '-');
            return $performance[0];
        }
        return null;
    }

    function getAllPerformances(string $type = null, string $week = null): ?array {
        $currentDate = $this->getCurrentDate();
        $staticEndpoint = config('app.static_endpoint');
        $typeQuery = '';
        $weekQuery = '';
        if ($type) {
            $typeQuery = "AND p.type = '". $type . "'";
        }
        if ($week) {
            $weekQuery = "AND p.week = '". $week . "'";
        }
        $performances =  DB::select(
            "
                SELECT
                    p.*,
                    CONCAT('".$staticEndpoint."', uf.url) AS thumbnail_url,
                    CASE
                        WHEN ('".$currentDate."' >= p.start_live AND '".$currentDate."' <= p.end_live) THEN 'live'
                        WHEN ('".$currentDate."' < p.start_live) THEN 'coming_soon'
                        WHEN ('".$currentDate."' > p.expired_date) THEN 'expired'
                        ELSE 'vod'
                    END AS performance_status_now
                FROM performances AS p
                LEFT JOIN upload_file_morph AS ufm ON ufm.related_type = 'performances' AND ufm.related_id = p.id
                LEFT JOIN upload_file AS uf ON uf.id = ufm.upload_file_id
                WHERE p.active_status = 1 AND (p.creators_park_exclusive != 1 OR p.creators_park_exclusive IS NULL) " . $typeQuery . " " . $weekQuery . "
                ORDER BY p.start_live ASC
            "
        );
        if (count($performances) > 0) {
            for ($x = 0; $x < count($performances); $x++) {
                $performances[$x]->title_slug = Str::slug($performances[$x]->title, '-');
            }
            return $performances;
        }
        return null;
    }

    function getFeaturedPerformance(): ?\stdClass
    {
        $performances = $this->getAllPerformances();
        if ($performances) {
            $len = count($performances);
            for ($x = 0; $x < $len; $x++) {
                switch ($performances[$x]->performance_status_now) {
                    case 'coming_soon':
                    case 'live':
                        return $performances[$x];
                    default:
                        if ($x === ($len - 1)) {
                            return $performances[$x];
                        }
                }
            }
        }
        return null;
    }

    function getThisWeekPerformancesDateGroup(string $week = null, string $type = null): ?array {
        $staticEndpoint = config('app.static_endpoint');
        $currentDate = $this->getCurrentDate();
        $currentWeek = $this->getCurrentWeek($week);
        $typeQuery = '';
        if ($type) {
            $typeQuery = "AND p.type = '" . $type . "'";
        }
        $performances =  DB::select(
            "
                SELECT
                    p.*,
                    CONCAT('".$staticEndpoint."', uf.url) AS thumbnail_url,
                    CASE
                        WHEN ('".$currentDate."' >= p.start_live AND '".$currentDate."' <= p.end_live) THEN 'live'
                        WHEN ('".$currentDate."' < p.start_live) THEN 'coming_soon'
                        WHEN ('".$currentDate."' > p.expired_date) THEN 'expired'
                        ELSE 'vod'
                    END AS performance_status_now
                FROM performances AS p
                LEFT JOIN upload_file_morph AS ufm ON ufm.related_type = 'performances' AND ufm.related_id = p.id
                LEFT JOIN upload_file AS uf ON uf.id = ufm.upload_file_id
                WHERE p.week = '".$currentWeek."' AND p.active_status = 1 AND (p.creators_park_exclusive != 1 OR p.creators_park_exclusive IS NULL) " . $typeQuery . "
                ORDER BY p.start_live ASC
            "
        );
        $performancesDateGroup = [];
        $len = count($performances);
        for ($x = 0; $x < $len; $x++) {
            $performances[$x]->title_slug = Str::slug($performances[$x]->title, '-');
            $dateKey = explode(' ', $performances[$x]->start_live)[0];
            if (!key_exists($dateKey, $performancesDateGroup)) {
                $performancesDateGroup[$dateKey] = [];
            }
            array_push($performancesDateGroup[$dateKey], $performances[$x]);
        }
        if (count($performancesDateGroup) > 0) {
            return $performancesDateGroup;
        }
        return null;
    }

    function getNextWeekPerformancesDateGroup(): ?array {
        $sep = '_';
        $currentWeek = $this->getCurrentWeek();
        $weekVal = explode($sep, $currentWeek);
        $nextWeek = $weekVal[0].$sep.((int)$weekVal[1] + 1);
        return $this->getThisWeekPerformancesDateGroup($nextWeek);
    }

    function getAllWeeksPerformancesDateGroup(string $type = null): ?array {
        $currentDate = $this->getCurrentDate();
        $allWeeksPerformances = [];
        $weeks = $this->getAllWeeksFromPerformances();
        if ($weeks) {
            foreach ($weeks as $key => $val) {
                $thisWeekPerformances = $this->getAllPerformances($type, $val);
                if ($thisWeekPerformances) {
                    $startDate = $thisWeekPerformances[0]->start_live;
                    $endDate = end($thisWeekPerformances)->start_live;
                    $expiredDate = end($thisWeekPerformances)->expired_date;
                    if ($currentDate > $expiredDate) {
                        $performancesStatusNow = 'expired';
                    } else if ($currentDate < $startDate) {
                        $performancesStatusNow = 'coming_soon';
                    } else {
                        $performancesStatusNow = 'available';
                    }
                    $allWeeksPerformances[$val]['performances_status_now'] = $performancesStatusNow;
                    $allWeeksPerformances[$val]['start_date'] = $startDate;
                    $allWeeksPerformances[$val]['end_date'] = $endDate;
                    $allWeeksPerformances[$val]['expired_date'] = $expiredDate;
                    $allWeeksPerformances[$val]['performances'] = $thisWeekPerformances;
                }
            }
        }
        if (count($allWeeksPerformances) > 0) {
            return $allWeeksPerformances;
        }
        return null;
    }

    function getCurrentDate(string $date = null): string {
        $cfg = $this->getConfig();
        $currentDate = date('Y-m-d H:i:s');
        if ($cfg && (
            (property_exists($cfg, 'simulation_mode') && $cfg->simulation_mode == 1) &&
            (property_exists($cfg, 'simulation_current_date') && $cfg->simulation_current_date)
        )) {
            $currentDate = $cfg->simulation_current_date;
        }
        if ($date) {
            $currentDate = $date;
        }
        return $currentDate;
    }

    function getCurrentWeek(string $week = null): string {
        $cfg = $this->getConfig();
        $currentWeek = 'week_1';
        if ($cfg && (
            (property_exists($cfg, 'simulation_mode') && $cfg->simulation_mode == 1) &&
            (property_exists($cfg, 'simulation_current_week') && $cfg->simulation_current_week)
        )) {
            $currentWeek = $cfg->simulation_current_week;
        } else if ($cfg && $cfg->current_week) {
            $currentWeek = $cfg->current_week;
        }
        if ($week) {
            $currentWeek = $week;
        }
        return $currentWeek;
    }

    function storePrivySubmissionLog(string $nik, string $status)
    {
        $redis = Redis::connection();
        $currentDate = date('Y-m-d H:i:s');
        $encodedNIK = md5($nik);
        $key = 'soundre:privy:'.$encodedNIK.':'.$status;
        try {
            $subLogs = json_decode($redis->get($key));
        } catch (\Exception $e) {
            $subLogs = null;
        }
        if ($subLogs && is_array($subLogs)) {
            array_push($subLogs, [
                'date' => $currentDate,
            ]);
            $redis->set($key, json_encode($subLogs));
        } else {
            $redis->set($key, json_encode([
                [
                    'date' => $currentDate,
                ]
            ]));
        }
    }

    function storePrivyLogToDB($uid_hashed, $privy_resp, $app_resp, $status)
    {
        DB::insert(
            "
                INSERT INTO log_privy (uid_hashed, privy_resp, app_resp, status) VALUES
                (?, ?, ?, ?)
            "
        , [
            $uid_hashed,
            json_encode($privy_resp),
            json_encode($app_resp),
            $status
        ]);
    }
}
