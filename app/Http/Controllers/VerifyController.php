<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class VerifyController extends Controller
{
    function verifyAge(string $nik, string $name, string $dob): array
    {
        $soundreDataController = new SoundreDataController();

        // Generates base response payload.
        $resJson = [
            'code' => '500',
            'message' => 'Something went wrong!',
            'data' => [
                'ext_code' => null,
                'ext_transactionId' => null,
                'ext_status' => null,
                'ext_ageStatus' => null,
                'ext_age' => null,
                'ext_message' => null
            ]
        ];

        // Validates request payload.
        // Return immediately if the request is not valid.
        if (!$nik) {
            $resJson['code'] = '400';
            $resJson['message'] = 'Bad request! `nik` is needed!';
            return $resJson;
        }
        if (!$name) {
            $resJson['code'] = '400';
            $resJson['message'] = 'Bad request! `name` is needed!';
            return $resJson;
        }
        if (!$dob) {
            $resJson['code'] = '400';
            $resJson['message'] = 'Bad request! `dob` is needed with format (DD-MM-YYYY)!';
            return $resJson;
        }

        // Integrates with external API (PrivyID) to check the age.
        $client = new Client();
        $hostname = config('privyid.hostname');
        $endpoint = config('privyid.merchant_verify_endpoint');
        $url = $hostname . $endpoint;
        $authUsername = config('privyid.username');
        $authPassword = config('privyid.password');
        $key = config('privyid.merchant_key');
        try {
            $res = $client->post($url,
                [
                    'auth' =>  [$authUsername, $authPassword],
                    'headers' => [
                        'Merchantkey' => $key
                    ],
                    'json' => [
                        'nik' => $nik,
                        'name' => $name,
                        'dob' => $dob
                    ],
                    'timeout' => floatval(config('app.privy_req_timeout')),
                    'connect_timeout' => floatval(config('app.privy_req_connect_timeout')),
                ]
            );
        } catch (BadResponseException $e) {
            $res = $e->getResponse();
        }

        // Checks & validates the external API (PrivyID) response.
        $resStatusCode = $res->getStatusCode();
        if ($resStatusCode == 200) {
            $resBody = json_decode($res->getBody());
            if ($resBody->code <= 99) {
                $resJson['data']['ext_code'] = $resBody->code;
                $resJson['data']['ext_message'] = $resBody->message;
                if (property_exists($resBody, 'transactionId')) {
                    $resJson['data']['ext_transactionId'] = $resBody->transactionId;
                }
                if (property_exists($resBody, 'status')) {
                    $resJson['data']['ext_status'] = $resBody->status;
                }
                if (property_exists($resBody, 'ageStatus')) {
                    $resJson['data']['ext_ageStatus'] = $resBody->ageStatus;
                }
                if (property_exists($resBody, 'age')) {
                    $resJson['data']['ext_age'] = $resBody->age;
                }
                if ($resBody->code == '00' && strtolower($resBody->status) == 'match' && strtolower($resBody->ageStatus) != 'match') {
                    $resJson['code'] = '01';
                    $resJson['message'] = 'Usia dibawah 18 tahun';
                    $soundreDataController->storePrivyLogToDB(md5($nik), $resBody, $resJson, 'failed');
                } else if ($resBody->code == '01') {
                    $resJson['code'] = '02';
                    $resJson['message'] = 'Data yang disubmit tidak sesuai. Mohon cek kembali data yang kamu masukkan';
                    $soundreDataController->storePrivyLogToDB(md5($nik), $resBody, $resJson, 'failed');
                } else if ($resBody->code == '99') {
                    $resJson['code'] = '03';
                    $resJson['message'] = 'Nomor KTP tidak ditemukan';
                    $soundreDataController->storePrivyLogToDB(md5($nik), $resBody, $resJson, 'failed');
                } else if ($resBody->code == '00' && strtolower($resBody->status) == 'match') {
                    $resJson['code'] = '00';
                    $resJson['message'] = 'Data berhasil diverifikasi';
                    $soundreDataController->storePrivyLogToDB(md5($nik), $resBody, $resJson, 'success');
                } else {
                    $resJson['code'] = '04';
                    $resJson['message'] = 'Data yang disubmit tidak sesuai. Mohon cek kembali data yang kamu masukkan';
                    $soundreDataController->storePrivyLogToDB(md5($nik), $resBody, $resJson, 'failed');
                }
            } else {
                $resJson['code'] = '-1';
                $resJson['message'] = 'Data yang disubmit tidak sesuai. Mohon cek kembali data yang kamu masukkan';
                $resJson['data']['ext_code'] = '' . $resBody->code;
                $resJson['data']['ext_message'] = $resBody->message;
                $soundreDataController->storePrivyLogToDB(md5($nik), $resBody, $resJson, 'failed');
            }
        } else {
            $resBody = json_decode($res->getBody()->getContents());
            $resJson['code'] = '-1';
            $resJson['message'] = 'Terjadi kendala saat verifikasi, silahkan coba lagi';
            $resJson['data']['ext_code'] = '' . $resStatusCode;
            $resJson['data']['ext_message'] = $resBody->message;
            $soundreDataController->storePrivyLogToDB(md5($nik), $resBody, $resJson, 'failed');
        }

        // Returns final response.
        return $resJson;
    }
}
