<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use Faker\Provider\id_ID\Person;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class SoundreController extends Controller
{
    /**
     * @var SoundreDataController
     */
    private $soundreDataController;
    /**
     * @var Repository|Application|mixed|string
     */
    private $staticAssetPrefix;
    /**
     * @var Repository|Application|mixed
     */
    private $gtmAuth;
    /**
     * @var Repository|Application|mixed
     */
    private $gtmId;
    /**
     * @var string
     */
    private $assetVersion;
    /**
     * @var Repository|Application|mixed
     */
    private $ga4Id;
    /**
     * @var Repository|Application|mixed
     */
    private $gaClassicVideoId;
    /**
     * @var Repository|Application|mixed
     */
    private $gaClassicAllId;
    /**
     * @var Repository|Application|mixed
     */
    private $gtmEnv;
    /**
     * @var string
     */
    private $pageUrl;
    /**
     * @var string
     */
    private $hostname;
    /**
     * @var Repository|Application|mixed
     */
    private $gtmDmpId;
    /**
     * @var bool
     */
    private $isLive;

    function __construct() {
        $this->soundreDataController = new SoundreDataController();
        $this->staticAssetPrefix = config('app.static_asset_prefix');
        if ($this->staticAssetPrefix === '/') {
            $this->staticAssetPrefix = '';
        }
        $this->hostname = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
        $this->pageUrl = $this->hostname . "$_SERVER[REQUEST_URI]";
        $this->ga4Id = config('app.ga_4_id');
        $this->gaClassicVideoId = config('app.ga_classic_video_id');
        $this->gaClassicAllId = config('app.ga_classic_all_id');
        $this->gtmId = config('app.gtm_id');
        $this->gtmEnv = config('app.gtm_env');
        $this->gtmAuth = config('app.gtm_auth');
        $this->gtmDmpId = config('app.gtm_dmp_id');
        $this->assetVersion = '20211102134848';
        $this->isLive = true;
        if (date('Y-m-d H:i:s') < config('app.soundre_live_at')) {
            $this->isLive = false;
        }
    }

    function homepage(Request $request): Response
    {
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        $enterEventSession = $request->session()->get('event-enter');
        if (!$enterEventSession) {
            $data = [
                'hostname' => $this->hostname,
                'pageUrl' => $this->pageUrl,
                'ga4Id' => $this->ga4Id,
                'gaClassicVideoId' => $this->gaClassicVideoId,
                'gaClassicAllId' => $this->gaClassicAllId,
                'gtmId' => $this->gtmId,
                'gtmEnv' => $this->gtmEnv,
                'gtmAuth' => $this->gtmAuth,
                'gtmDmpId' => $this->gtmDmpId,
                'assetVersion' => $this->assetVersion,
                'isLive' => $this->isLive,
                'meta' => $this->soundreDataController->getPageMetaByType('unbranded'),
                'staticAssetPrefix' => $this->staticAssetPrefix,
            ];
            return response()
                ->view('unbranded', $data)
                ->header('X-Powered-By', 'Dark Phoenix');
        } else {
            $verifyAgeSession = $request->session()->get('verify-age');
            if (!$verifyAgeSession || $verifyAgeSession['code'] != '00') {
                $verifyAgeSession = null;
            }
            $data = [
                'hostname' => $this->hostname,
                'pageUrl' => $this->pageUrl,
                'ga4Id' => $this->ga4Id,
                'gaClassicVideoId' => $this->gaClassicVideoId,
                'gaClassicAllId' => $this->gaClassicAllId,
                'gtmId' => $this->gtmId,
                'gtmEnv' => $this->gtmEnv,
                'gtmAuth' => $this->gtmAuth,
                'gtmDmpId' => $this->gtmDmpId,
                'assetVersion' => $this->assetVersion,
                'verifyAgeSession' => $verifyAgeSession,
                'isLive' => $this->isLive,
                'previousPage' => $this->getPreviousUrl($request),
                'meta' => $this->soundreDataController->getPageMetaByType('homepage'),
                'staticAssetPrefix' => $this->staticAssetPrefix,
                'homepageBanners' => $this->soundreDataController->getAllHomepageBanners(),
                'allWeeksSpecialPerformancesDateGroup' =>
                    $this->soundreDataController->getAllWeeksPerformancesDateGroup('special')
            ];
            return response()
                ->view('homepage', $data)
                ->header('X-Powered-By', 'Dark Phoenix');
        }
    }

    function creatorsPark(Request $request)
    {
        $creatorsParkLoc = $request->query('loc', '');
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        $enterEventSession = $request->session()->get('event-enter');
        if (!$enterEventSession) {
            return redirect('/soundrenaline');
        }
        $creatorsParkLoc = $request->query('loc', '');
        $creatorsParkAuth = $request->query('auth', '');
        $creatorsParkUrl = '';
        $verifyAgeSession = $request->session()->get('verify-age');
        if (!$verifyAgeSession || $verifyAgeSession['code'] != '00') {
            $verifyAgeSession = null;
        } else {
            $creatorsParkData = base64_encode(
                'name=' . urlencode($verifyAgeSession['name']) . '&sessionId=' . $request->session()->getId()
            );
            $creatorsParkUrl = config('app.creators_park_url') . $creatorsParkLoc . '?data=' . $creatorsParkData . '&auth=' . $creatorsParkAuth;
        }
        $data = [
            'hostname' => $this->hostname,
            'pageUrl' => $this->pageUrl,
            'ga4Id' => $this->ga4Id,
            'gaClassicVideoId' => $this->gaClassicVideoId,
            'gaClassicAllId' => $this->gaClassicAllId,
            'gtmId' => $this->gtmId,
            'gtmEnv' => $this->gtmEnv,
            'gtmAuth' => $this->gtmAuth,
            'gtmDmpId' => $this->gtmDmpId,
            'assetVersion' => $this->assetVersion,
            'verifyAgeSession' => $verifyAgeSession,
            'isLive' => $this->isLive,
            'previousPage' => '/soundrenaline',
            'meta' => $this->soundreDataController->getPageMetaByType('creators_park'),
            'staticAssetPrefix' => $this->staticAssetPrefix,
            'creatorsParkUrl' => $creatorsParkUrl,
        ];
        if ($this->isLive) {
            return response()
                ->view('creatorspark', $data)
                ->header('X-Powered-By', 'Dark Phoenix');
        } else {
            return response()
                ->view('comingsoon', $data)
                ->header('X-Powered-By', 'Dark Phoenix');
        }
    }

    function performance(Request $request, string $id = null, string $titleSlug = null)
    {
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        $enterEventSession = $request->session()->get('event-enter');
        if (!$enterEventSession) {
            return redirect('/soundrenaline');
        }
        $verifyAgeSession = $request->session()->get('verify-age');
        if (!$verifyAgeSession || $verifyAgeSession['code'] != '00') {
            $verifyAgeSession = null;
        }
        if ($id && is_numeric($id)) {
            $currentPlayedPerformance = $this->soundreDataController->getCurrentPlayedPerformanceById((int)($id));
            if (
                $currentPlayedPerformance &&
                (
                    $currentPlayedPerformance->performance_status_now === 'live' ||
                    $currentPlayedPerformance->performance_status_now === 'vod'
                )
            ) {
                $data = [
                    'playedPerformance' => true,
                    'hostname' => $this->hostname,
                    'pageUrl' => $this->pageUrl,
                    'ga4Id' => $this->ga4Id,
                    'gaClassicVideoId' => $this->gaClassicVideoId,
                    'gaClassicAllId' => $this->gaClassicAllId,
                    'gtmId' => $this->gtmId,
                    'gtmEnv' => $this->gtmEnv,
                    'gtmAuth' => $this->gtmAuth,
                    'gtmDmpId' => $this->gtmDmpId,
                    'assetVersion' => $this->assetVersion,
                    'verifyAgeSession' => $verifyAgeSession,
                    'isLive' => $this->isLive,
                    'previousPage' => $this->getPreviousUrl($request),
                    'meta' => $this->soundreDataController->getPageMetaByType('performance'),
                    'staticAssetPrefix' => $this->staticAssetPrefix,
                    'currentPlayedPerformance' => $currentPlayedPerformance,
                    'thisWeekPerformancesDateGroup' => $this->soundreDataController->getThisWeekPerformancesDateGroup(),
                ];
            } else {
                $data = [
                    'playedPerformance' => false,
                    'hostname' => $this->hostname,
                    'pageUrl' => $this->pageUrl,
                    'currentDate' => $this->soundreDataController->getCurrentDate(),
                    'ga4Id' => $this->ga4Id,
                    'gaClassicVideoId' => $this->gaClassicVideoId,
                    'gaClassicAllId' => $this->gaClassicAllId,
                    'gtmId' => $this->gtmId,
                    'gtmEnv' => $this->gtmEnv,
                    'gtmAuth' => $this->gtmAuth,
                    'gtmDmpId' => $this->gtmDmpId,
                    'assetVersion' => $this->assetVersion,
                    'verifyAgeSession' => $verifyAgeSession,
                    'isLive' => $this->isLive,
                    'previousPage' => $this->getPreviousUrl($request),
                    'meta' => $this->soundreDataController->getPageMetaByType('performance'),
                    'staticAssetPrefix' => $this->staticAssetPrefix,
                    'specialPerformances' => $this->soundreDataController->getAllPerformances('special'),
                    'featuredPerformance' => $this->soundreDataController->getFeaturedPerformance(),
                    'thisWeekPerformancesDateGroup' => $this->soundreDataController->getThisWeekPerformancesDateGroup(),
                    'allWeeksFromPerformances' => $this->soundreDataController->getAllWeeksFromPerformances(),
                    'allWeeksPerformancesDateGroup' => $this->soundreDataController->getAllWeeksPerformancesDateGroup(),
                    'currentWeek' => $this->soundreDataController->getCurrentWeek(),
                ];
            }
        } else {
            $data = [
                'playedPerformance' => false,
                'hostname' => $this->hostname,
                'pageUrl' => $this->pageUrl,
                'currentDate' => $this->soundreDataController->getCurrentDate(),
                'ga4Id' => $this->ga4Id,
                'gaClassicVideoId' => $this->gaClassicVideoId,
                'gaClassicAllId' => $this->gaClassicAllId,
                'gtmId' => $this->gtmId,
                'gtmEnv' => $this->gtmEnv,
                'gtmAuth' => $this->gtmAuth,
                'gtmDmpId' => $this->gtmDmpId,
                'assetVersion' => $this->assetVersion,
                'verifyAgeSession' => $verifyAgeSession,
                'isLive' => $this->isLive,
                'previousPage' => $this->getPreviousUrl($request),
                'meta' => $this->soundreDataController->getPageMetaByType('performance'),
                'staticAssetPrefix' => $this->staticAssetPrefix,
                'specialPerformances' => $this->soundreDataController->getAllPerformances('special'),
                'featuredPerformance' => $this->soundreDataController->getFeaturedPerformance(),
                'thisWeekPerformancesDateGroup' => $this->soundreDataController->getThisWeekPerformancesDateGroup(),
                'allWeeksFromPerformances' => $this->soundreDataController->getAllWeeksFromPerformances(),
                'allWeeksPerformancesDateGroup' => $this->soundreDataController->getAllWeeksPerformancesDateGroup(),
                'currentWeek' => $this->soundreDataController->getCurrentWeek(),
            ];
        }
        return response()
            ->view('performance', $data)
            ->header('X-Powered-By', 'Dark Phoenix');
    }

    function schedule(Request $request)
    {
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        $enterEventSession = $request->session()->get('event-enter');
        if (!$enterEventSession) {
            return redirect('/soundrenaline');
        }
        $verifyAgeSession = $request->session()->get('verify-age');
        if (!$verifyAgeSession || $verifyAgeSession['code'] != '00') {
            $verifyAgeSession = null;
        }
        $data = [
            'playedPerformance' => false,
            'hostname' => $this->hostname,
            'pageUrl' => $this->pageUrl,
            'currentDate' => $this->soundreDataController->getCurrentDate(),
            'ga4Id' => $this->ga4Id,
            'gaClassicVideoId' => $this->gaClassicVideoId,
            'gaClassicAllId' => $this->gaClassicAllId,
            'gtmId' => $this->gtmId,
            'gtmEnv' => $this->gtmEnv,
            'gtmAuth' => $this->gtmAuth,
            'gtmDmpId' => $this->gtmDmpId,
            'assetVersion' => $this->assetVersion,
            'verifyAgeSession' => $verifyAgeSession,
            'isLive' => $this->isLive,
            'previousPage' => $this->getPreviousUrl($request),
            'meta' => $this->soundreDataController->getPageMetaByType('schedule'),
            'staticAssetPrefix' => $this->staticAssetPrefix,
            'allWeeksFromPerformances' => $this->soundreDataController->getAllWeeksFromPerformances(),
            'allWeeksPerformancesDateGroup' => $this->soundreDataController->getAllWeeksPerformancesDateGroup(),
            'currentWeek' => $this->soundreDataController->getCurrentWeek(),
        ];
        return response()
            ->view('schedule', $data)
            ->header('X-Powered-By', 'Dark Phoenix');
    }

    function contactUs(Request $request)
    {
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        $enterEventSession = $request->session()->get('event-enter');
        if (!$enterEventSession) {
            return redirect('/soundrenaline');
        }
        $verifyAgeSession = $request->session()->get('verify-age');
        if (!$verifyAgeSession || $verifyAgeSession['code'] != '00') {
            $verifyAgeSession = null;
        }
        $data = [
            'hostname' => $this->hostname,
            'pageUrl' => $this->pageUrl,
            'ga4Id' => $this->ga4Id,
            'gaClassicVideoId' => $this->gaClassicVideoId,
            'gaClassicAllId' => $this->gaClassicAllId,
            'gtmId' => $this->gtmId,
            'gtmEnv' => $this->gtmEnv,
            'gtmAuth' => $this->gtmAuth,
            'gtmDmpId' => $this->gtmDmpId,
            'assetVersion' => $this->assetVersion,
            'verifyAgeSession' => $verifyAgeSession,
            'isLive' => $this->isLive,
            'previousPage' => $this->getPreviousUrl($request),
            'meta' => $this->soundreDataController->getPageMetaByType('contact_us'),
            'staticAssetPrefix' => $this->staticAssetPrefix
        ];
        return response()
            ->view('contactus', $data)
            ->header('X-Powered-By', 'Dark Phoenix');
    }

    function verifyAgeSubmit(Request $request): JsonResponse
    {
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        try {
            $reqJson = json_decode($request->getContent(), true);
            $nik = $reqJson['nik'];
            $name = ucwords(strtolower($reqJson['name']));
            $dob = $reqJson['dob'];
            $verifyController = new VerifyController();
            $resBody = $verifyController->verifyAge($nik, $name, $dob);
            $request->session()->put('verify-age', [
                'name' => $name,
                'code' => $resBody['code'],
                'message' => $resBody['message']
            ]);
            if ($resBody['code'] === '00') {
                $this->soundreDataController->storePrivySubmissionLog($nik, 'success');
            } else {
                $this->soundreDataController->storePrivySubmissionLog($nik, 'failed');
            }
            return response()->json($resBody, 200)->header('X-Robots-Tag', 'noindex');
        } catch (Exception $e) {
            return response()->json([
                'code' => '-99',
                'message' => 'Sedang terjadi kendala, silahkan coba lagi',
                'data' => []
            ], 200)->header('X-Robots-Tag', 'noindex');
        }
    }

    function bypassAgeVerification(Request $request)
    {
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        try {
//            $nik = '999' . rand(1000000000,9999999999) . '999';
            $name = ucwords(Person::firstNameMale() . ' ' . Person::lastNameMale());
//            $dob = '1990-' . str_pad(rand(1, 10),2,"0",STR_PAD_LEFT) . '-' .
//                str_pad(rand(1, 10),2,"0",STR_PAD_LEFT);
            $request->session()->put('verify-age', [
                'name' => $name,
                'code' => '00',
                'message' =>'bypass ok'
            ]);
            $this->setEventEnterSession($request);
            return redirect('/soundrenaline');
        } catch (Exception $e) {
            return redirect('/soundrenaline');
        }
    }

    function bypassUnbranded(Request $request)
    {
        $this->deleteSessionAfterSoundreEnd($request);
        $this->deleteEventEnterSessionAfterExpired($request);
        try {
            $this->setEventEnterSession($request);
            return redirect('/soundrenaline');
        } catch (Exception $e) {
            return redirect('/soundrenaline');
        }
    }

    function enterEvent(Request $request): JsonResponse
    {
        $this->setEventEnterSession($request);
        return response()->json([
            'code' => '00',
            'message' => 'ok',
            'data' => []
        ], 200)->header('X-Robots-Tag', 'noindex');
    }

    function setEventEnterSession(Request $request)
    {
        $now = new DateTime();
        $sessionLifeTime = config('app.soundre_cookie_session_lifetime');
        $expiredAt = $now->modify('+'.$sessionLifeTime.' seconds')->format('Y-m-d H:i:s');
        $request->session()->put('event-enter', [
            'expired_at' => $expiredAt,
        ]);
    }

    function deleteSessionAfterSoundreEnd(Request $request)
    {
        $currentDate = date('Y-m-d H:i:s');
        $sessionDateExpired = config('app.destroy_soundre_session_at');
        if ($currentDate > $sessionDateExpired) {
            $request->session()->forget('verify-age');
        }
    }

    function deleteEventEnterSessionAfterExpired(Request $request)
    {
        $eventEnterSession = $request->session()->get('event-enter');
        if ($eventEnterSession && array_key_exists('expired_at', $eventEnterSession)) {
            $currentDate = date('Y-m-d H:i:s');
            if ($currentDate > $eventEnterSession['expired_at']) {
                $request->session()->forget('event-enter');
            }
        }
    }

    function getPreviousUrl(Request $request): string
    {
        $prevUrl = str_replace(url('/'), '', url()->previous());
        return ($prevUrl !== '') ? $prevUrl : '/soundrenaline';
    }

    function privySubmissionInfo()
    {
        dd('total submission status', $this->soundreDataController->getPrivySubmissionStatuses());
    }

    // Functions bellow is just for testing purpose.

    function testVerify(Request $request)
    {
        $submitUrl = '/soundrenaline/test-verify/submit';
        $redirectURLIfAllowed = '/soundrenaline';
        $verifyAgeSession = $request->session()->get('verify-age');
        if ($verifyAgeSession && $verifyAgeSession['code'] == '00') {
            return redirect($redirectURLIfAllowed);
        } else {
            $data = [
                'submitUrl' => $submitUrl,
                'redirectURLIfAllowed' => $redirectURLIfAllowed
            ];
            return response()
                ->view('testverify', $data)
                ->header('X-Powered-By', 'Dark Phoenix');
        }
    }

    function testVerifySubmit(Request $request): JsonResponse
    {
        $reqJson = json_decode($request->getContent(), true);
        $nik = $reqJson['nik'];
        $name = $reqJson['name'];
        $dob = $reqJson['dob'];
        $verifyController = new VerifyController();
        $resBody = $verifyController->verifyAge($nik, $name, $dob);
        $request->session()->put('verify-age', [
            'name' => $name,
            'code' => $resBody['code'],
            'message' => $resBody['message']
        ]);
        return response()->json($resBody, 200)->header('X-Robots-Tag', 'noindex');
    }

    function testLogout(Request $request)
    {
        $redirectURLIfAllowed = '/soundrenaline';
        $request->session()->remove('verify-age');
        return redirect($redirectURLIfAllowed);
    }

    function testData(Request $request, string $id = null)
    {
        $verifyAgeSession = $request->session()->get('verify-age');
        if ($id && is_numeric($id)) {
            $currentPlayedPerformance = $this->soundreDataController->getCurrentPlayedPerformanceById((int)($id));
        } else {
            $currentPlayedPerformance = $this->soundreDataController->getCurrentPlayedPerformanceById(5);
        }
        $data = [
            'currentDate' => $this->soundreDataController->getCurrentDate(),
            'currentWeek' => $this->soundreDataController->getCurrentWeek(),
            'sessionId' => $request->session()->getId(),
            'verifyAgeSession' => $verifyAgeSession,
            'isLive' => $this->isLive,
            'previousPage' => $this->getPreviousUrl($request),
            'homepageMeta' => $this->soundreDataController->getPageMetaByType('homepage'),
            'performanceMeta' => $this->soundreDataController->getPageMetaByType('performance'),
            'contactUsMeta' => $this->soundreDataController->getPageMetaByType('contact_us'),
            'creatorsParkMeta' => $this->soundreDataController->getPageMetaByType('creators_park'),
            'homepageBanners' => $this->soundreDataController->getAllHomepageBanners(),
            'currentPlayedPerformance' => $currentPlayedPerformance,
            'specialPerformances' => $this->soundreDataController->getAllPerformances('special'),
            'featuredPerformance' => $this->soundreDataController->getFeaturedPerformance(),
            'thisWeekPerformancesDateGroup' => $this->soundreDataController->getThisWeekPerformancesDateGroup(),
            'nextWeekPerformancesDateGroup' => $this->soundreDataController->getNextWeekPerformancesDateGroup(),
            'allWeeksFromPerformances' => $this->soundreDataController->getAllWeeksFromPerformances(),
            'allWeeksPerformancesDateGroup' => $this->soundreDataController->getAllWeeksPerformancesDateGroup(),
            'allWeeksSpecialPerformancesDateGroup' => $this->soundreDataController->getAllWeeksPerformancesDateGroup('special')
        ];
        dd($data);
    }
}
