<?php

return [
    'username' => env('PRIVYID_USERNAME'),
    'password' => env('PRIVYID_PASSWORD'),
    'merchant_key' => env('PRIVYID_MERCHANT_KEY'),
    'hostname' => env('PRIVYID_HOSTNAME'),
    'merchant_verify_endpoint' => env('PRIVYID_MERCHANT_VERIFY_ENDPOINT'),
];
