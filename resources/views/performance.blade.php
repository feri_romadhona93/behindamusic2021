<!DOCTYPE html>
<html lang="en">
    <head>
        <script>let pageActiveTab = 'performance';</script>


        <!-- Global site tag (gtag.js) - Google Analytics GA Classic Pillar Video -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$gaClassicVideoId}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$gaClassicVideoId}}');
        </script>


        <!-- Global site tag (gtag.js) - Google Analytics GA 4 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$ga4Id}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$ga4Id}}');
        </script>


        <!-- Google Tag Manager -->
        <script>
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'pillar' : 'video'
            });
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth={{$gtmAuth}}'+
                '&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','{{$gtmId}}');
        </script>


        <!-- Google Tag Manager - DMP -->
        <script>
            window.dataLayerDMP = window.dataLayerDMP || [];
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore( j,f);
            })(window,document,'script','dataLayerDMP','{{$gtmDmpId}}');
        </script>


        <!-- comScore -->
        <script>
            let _comscore = [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
                let s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>


        <!-- Alexa Certify -->
        <script type="text/javascript">
            _atrk_opts = { atrk_acct:"8oNJt1FYxz20cv", domain:"rctiplus.com",dynamic: true};
            (function() { let as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; let s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>


        <!-- Tracker function -->
        <script>
            function tracker (event_category, event_action, event_label) {
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                });
            }
            window.tracker = tracker;
            function trackerBanner(event_category, event_action, event_label, id, name, type) {
                let video_category = 'not_available';
                let video_content_type = 'photo';
                if (type === 'video') {
                    video_category = 'vod';
                    video_content_type = 'clip';
                }
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                    'banner_id': id,
                    'banner_name': name,
                    'content_id': 'not_available',
                    'content_name': 'not_available',
                    'video_channel_id': 'not_available',
                    'program_id': 'not_available',
                    'program_name': 'not_available',
                    'video_category': video_category,
                    'video_content_type': video_content_type,
                });
            }
            window.trackerBanner = trackerBanner;
            function trackerPlayer(event_category, event_action, event_label, id, title, name, status, date) {
                let video_content_type = 'live event';
                if (status !== 'live') {
                    video_content_type = 'missed event';
                }
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                    'content_id': id,
                    'content_name': title,
                    'video_channel_id': id,
                    'program_id': 'not_available',
                    'program_name': 'not_available',
                    'video_category': status,
                    'video_content_type': video_content_type,
                    'cast': name,
                    'event_schedule_time': date,
                });
            }
            window.trackerPlayer = trackerPlayer;
        </script>


        <!-- Tracker function - DMP -->
        <script>
            function trackerDMPCookie (cookieValue = 'opt-out') {
                window.dataLayerDMP.push({
                    'event': 'cookieSettings',
                    'cookieType': 'advertising',
                    'cookieValue': cookieValue
                });
            }
            function checkDwCookie(name) {
                let ca = document.cookie.split(';');
                for(let i = 0; i < ca.length; i++) {
                    let c = ca[i];
                    while (c.charAt(0) === ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) === 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }
            function setDwAdvertisementCookie(triggerTracker = false, value = 'opt-out', exdays = 365, init = false) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                if (checkDwCookie("dw_Advertisement_cookie") === "") {
                    document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                    if (triggerTracker === true) {
                        trackerDMPCookie(value);
                    }
                } else {
                    if (init === false) {
                        document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                        if (triggerTracker === true) {
                            trackerDMPCookie(value);
                        }
                    }
                }
            }
            function setDwUserConsent(value = 'false', exdays = 365) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                document.cookie = 'dw_user_consent=' + value + ";" + expires + ";path=/soundrenaline";
            }
            setDwAdvertisementCookie(false, 'opt-out', 365, true);
            window.trackerDMPCookie = trackerDMPCookie;
        </script>


        <!-- Meta tags -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{$staticAssetPrefix}}/static/css/index.css?v={{$assetVersion}}" rel="stylesheet" />


        <?php
            $metaTitle = "Performances | Soundrenaline";
            $metaDesc = null;
            $metaKeywords = null;
            $metaImage = null;
            $metaImageAlt = null;
            $metaUploadDate = "2021-09-15T08:00:00+07:00";
            $metaDuration = "PT12H";
            if ($playedPerformance) {
                $metaUploadDate = date('Y-m-d\TH:i:s+07:00', strtotime($currentPlayedPerformance->start_live));
                try {
                    $metaDuration = "PT";
                    $inHour = (int)floor((int)($currentPlayedPerformance->duration_in_seconds)/3600);
                    $inMinute = (int)floor(((int)($currentPlayedPerformance->duration_in_seconds)%3600)/60);
                    $inSecond = (int)floor(((int)($currentPlayedPerformance->duration_in_seconds)%60));
                    $metaDuration = $metaDuration . $inHour . "H" . $inMinute . "M" . $inSecond . "S";
                } catch (Exception $e) {
                    $metaDuration = "PT12H";
                }
                $metaTitle = $currentPlayedPerformance->meta_title;
                $metaDesc = $currentPlayedPerformance->meta_description;
                $metaKeywords = $currentPlayedPerformance->meta_keywords;
                $metaImage = $currentPlayedPerformance->thumbnail_url;
                $metaImageAlt = $currentPlayedPerformance->meta_image_alt;
            } else if ($meta) {
                $metaTitle = $meta->meta_title;
                $metaDesc = $meta->meta_description;
                $metaKeywords = $meta->meta_keywords;
                $metaImage = $meta->meta_image_url;
                $metaImageAlt = $meta->meta_image_alt;
            }
        ?>


        <!-- SEO meta tags -->
        <title>{{$metaTitle}}</title>
        <link rel="icon" type="image/png" href="/assets/image/elements/favicon.png">
        <meta name="title" content="{{$metaTitle}}">
        <meta name="description" content="{{$metaDesc}}">
        <meta name="keywords" content="{{$metaKeywords}}">
        <meta property="og:title" content="{{$metaTitle}}">
        <meta property="og:description" content="{{$metaDesc}}">
        <meta property="og:type" content="website">
        <meta property="og:url" content="{{$pageUrl}}">
        <meta property="og:image" content="{{$metaImage}}">
        <meta property="fb:app_id" content="211272363627736">
        <meta property="og:site_name" content="RCTI+">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@RCTIPlus">
        <meta name="twitter:creator" content="@RCTIPlus">
        <meta name="twitter:title" content="{{$metaTitle}}">
        <meta name="twitter:description" content="{{$metaDesc}}">
        <meta name="twitter:image" content="{{$metaImage}}">
        <meta name="twitter:image:alt" content="{{$metaImageAlt}}">
        <meta name="twitter:url" content="{{$pageUrl}}">
        <meta name="twitter:domain" content="{{$pageUrl}}">


        <!-- JSON-LD -->
        <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "VideoObject",
                "name": "{{$metaTitle}}",
                "sameAs": [
                    "https://soundrenaline.id/",
                    "https://g.co/kgs/m35riY",
                    "https://www.instagram.com/soundrenaline.co.id",
                    "https://twitter.com/SOUNDRENALlNE"
                ],
                "description": "{{$metaDesc}}",
                "thumbnailUrl": "{{$metaImage}}",
                "uploadDate": "{{$metaUploadDate}}",
                "duration": "{{$metaDuration}}",
                "contentUrl": "{{$pageUrl}}",
                "interactionStatistic": {
                    "@type": "InteractionCounter",
                    "interactionType": { "@type": "WatchAction" },
                    "userInteractionCount": 99419
                }
            }
        </script>


        <!-- Internal style -->
        <style>
            @font-face {
                font-family: gopher;
                src: url({{$staticAssetPrefix}}/static/fonts/Gopher-Heavy.otf);
            }

            @font-face {
                font-family: saphiro-nrm;
                src: url({{$staticAssetPrefix}}/static/fonts/Shapiro\ 35\ Feather.otf);
            }

            @font-face {
                font-family: saphiro-hvy;
                src: url({{$staticAssetPrefix}}/static/fonts/Shapiro\ 75\ Heavy\ Text.otf);
            }

            #app {
                overflow-x: hidden;
                z-index: 10;
                background-image: url({{$staticAssetPrefix}}/static/images/bg_main_desktop.svg);
                background-position: top;
                background-repeat: repeat-y;
                background-size: cover;
                position: relative;
            }

            .card_popup {
                height: 100%;
                width: 100%;
                display: none;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                border-radius: 1rem;
                position: absolute;
                z-index: 10;
                background: url({{$staticAssetPrefix}}/static/icons/soonavailable.png);
                background-repeat: no-repeat;
                background-size: cover;
                background-position: center;
            }

            .icon_vod {
                background: url({{$staticAssetPrefix}}/static/icons/vod.svg);
                background-position: center;
                background-repeat: no-repeat;
                background-size: contain;
            }

            .vidplayer_schedule > .live + .schedule_icon {
                background: url({{$staticAssetPrefix}}/static/icons/sound.svg);
                background-position: center;
                background-repeat: no-repeat;
                background-size: contain;
            }

            .vidplayer_schedule > .vod + .schedule_icon {
                background: url({{$staticAssetPrefix}}/static/icons/vod.svg);
                background-position: center;
                background-repeat: no-repeat;
                background-size: contain;
            }

            .vidplayer_schedule > .playing + .schedule_icon {
                background: url({{$staticAssetPrefix}}/static/icons/pause.svg);
                background-position: center;
                background-repeat: no-repeat;
                background-size: contain;
            }
        </style>


        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/css/datepicker.min.css">
        <script src="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/js/datepicker.min.js"></script>
    </head>
    <body>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmId}}&gtm_auth={{$gtmAuth}}&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmDmpId}}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1">
        </noscript>
        <noscript>
            <img src="https://certify.alexametrics.com/atrk.gif?account=8oNJt1FYxz20cv" style="display:none" height="1" width="1" alt="" />
        </noscript>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script type="application/javascript" src="{{$staticAssetPrefix}}/static/javascript/index.min.js?v={{$assetVersion}}"></script>


        <!-- Loader -->
        <div id="loader">
            <img src="{{$staticAssetPrefix}}/static/icons/rctiplus.png" width="auto" height="auto" alt="soundrenaline 2021" />
            <div>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>


        <!-- Age verification -->
        @if (!$verifyAgeSession)
            <section class="modal_privy">
                <section class="modal_wrapper">
                    <header>
                        <img
                            class="privy_md_desktop"
                            src="{{$staticAssetPrefix}}/static/images/privy_banner_md_desktop.png"
                            alt="privy image"
                            width="500"
                            height="200" />
                        <p>Situs ini memiliki informasi mengenai produk yang mengandung tembakau dan hanya diperuntukkan bagi perokok berusia 18+ yang berdomisili di Indonesia.</p>
                    </header>
                    <main>
                        <h3>MOHON ISI DATA DIBAWAH INI</h3>
                        <span class="error_msg" style="display: block;"></span>
                        <form id="register_privy">
                            <div class="input_wrapper">
                                <label>Nama lengkap sesuai KTP</label>
                                <div class="input_field">
                                    <input
                                        id="fullname_field"
                                        required
                                        type="text"
                                        placeholder="Masukkan nama"
                                    />
                                </div>
                            </div>
                            <div class="input_wrapper">
                                <label>Nomor KTP</label>
                                <div class="input_field">
                                    <input
                                        id="idcardno_field"
                                        required
                                        type="number"
                                        maxlength="16"
                                        onkeydown="validateNumber(this)"
                                        placeholder="Contoh 16293001289841090"
                                    />
                                </div>
                            </div>
                            <div class="input_wrapper">
                                <label>Tanggal Lahir Sesuai KTP</label>
                                <div class="input_field">
                                    <input
                                        id="birthdate_field"
                                        placeholder="Masukkan tanggal lahir"
                                        inputmode="none"
                                        autocomplete="off"
                                        required />
                                </div>
                            </div>
                            <button class="link_btn">SUBMIT</button>
                        </form>
                    </main>
                    <footer>
                        <p>
                            Hubungi Customer Service Kami Melalui <br />
                            Email: <strong class="txt-bold" style="color: #434343;">CS@RCTIPLUS.COM</strong> atau WA number <br>
                            <strong class="txt-bold" style="color: #434343;">08888988880</strong>
                        </p>
                    </footer>
                </section>
            </section>
        @endif


        <main id="app">


            <!-- Navbar header -->
            <header id="header_content">
                <div id="header" class="header">
                    <figure class="nav_logo">
                        <h1>
                            <img
                                class="collaboration_logo"
                                src="{{$staticAssetPrefix}}/static/icons/logo_x_white.png"
                                width="150"
                                height="auto"
                                alt="soundrenaline 2021" />
                        </h1>
                    </figure>
                    <div class="mobile_back">
                        <a href="{{$previousPage}}">
                            <img
                                src="{{$staticAssetPrefix}}/static/icons/back_btn.svg"
                                role="button"
                                style="cursor: pointer;"
                                alt="back button" />
                        </a>
                        <h1>
                            <img
                                class="collaboration_logo"
                                src="{{$staticAssetPrefix}}/static/icons/logo_x_black.png"
                                width="100rem"
                                height="auto"
                                alt="soundrenaline 2021" />
                        </h1>
                    </div>
                    <nav>
                        <ul class="nav">
                            <li>
                                <a
                                    title="index"
                                    href="/soundrenaline"
                                    onclick="tracker('special_event_interaction', 'click_top_menu', 'home');"
                                >
                                    Home
                                </a>
                                <span></span>
                            </li>
                            <li>
                                <a
                                    title="performance"
                                    href="/soundrenaline/performance"
                                    onclick="tracker('special_event_interaction', 'click_top_menu', 'performance');"
                                >
                                    Performance
                                </a>
                                <span></span>
                            </li>
                            <li>
                                <a
                                    title="schedule"
                                    href="/soundrenaline/schedule"
                                    onclick="tracker('special_event_interaction', 'click_top_menu', 'schedule');"
                                >
                                    Schedule
                                </a>
                                <span></span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>


            <!-- If player -->
            @if ($playedPerformance)
                <?php
                    $id = $currentPlayedPerformance->id;
                    $performanceStatusNow = $currentPlayedPerformance->performance_status_now;
                    $thumbnailUrl = $currentPlayedPerformance->thumbnail_url;
                    if (!$thumbnailUrl) {
                        $thumbnailUrl = $staticAssetPrefix. '/static/icons/placeholder.svg';
                    }
                    $link = $currentPlayedPerformance->link;
                    $performerName = strtoupper($currentPlayedPerformance->performer_name);
                    $description = $currentPlayedPerformance->description;
                    $startLiveTime = explode(':', explode(' ', $currentPlayedPerformance->start_live)[1]);
                    $startLiveTime = $startLiveTime[0] . '.' . $startLiveTime[1];
                    $endLiveTime = explode(':', explode(' ', $currentPlayedPerformance->end_live)[1]);
                    $endLiveTime = $endLiveTime[0] . '.' . $endLiveTime[1];
                    $startLiveDate = explode(' ', $currentPlayedPerformance->start_live)[0];
                    $startLiveDate = date("j F Y", strtotime($startLiveDate));
                    $expiredDate = explode(' ', $currentPlayedPerformance->expired_date)[0];
                    $expiredDate = strtoupper(date("j F Y", strtotime($expiredDate)));
                    //$liveDate = strtoupper($startLiveTime . ' - ' . $endLiveTime . ' / ' . $startLiveDate);

                    // revision
                    $sDateSplit = explode('-', explode(' ', $currentPlayedPerformance->start_live)[0]);
                    $eDateSplit = explode('-', explode(' ', $currentPlayedPerformance->expired_date)[0]);
                    if ($sDateSplit[1] === $eDateSplit[1]) {
                        if ($sDateSplit[2] === $eDateSplit[2]) {
                            $liveDate = date("j F Y", strtotime(explode(' ', $currentPlayedPerformance->start_live)[0]));
                        } else {
                            $liveDate = date("j", strtotime(explode(' ', $currentPlayedPerformance->start_live)[0])).' - '.date("j F Y", strtotime(explode(' ', $currentPlayedPerformance->expired_date)[0]));
                        }
                    } else {
                        $liveDate = date("j F Y", strtotime(explode(' ', $currentPlayedPerformance->start_live)[0])).' - '.date("j F Y", strtotime(explode(' ', $currentPlayedPerformance->expired_date)[0]));
                    }
                    $liveDate = strtoupper($liveDate);
                ?>
                <section id="player_page">
                    <section class="panels">
                        <header>
                            <h1 class="section_title text_xl">WATCH PERFORMANCE</h1>
                        </header>
                        <main class="player_wrapper">
                            <section class="vid_player">
                                <iframe
                                    src="{{$link}}"
                                    width="auto"
                                    height="auto"
                                    scrolling="no"
                                    frameborder="0"
                                    allowfullscreen
                                    allowtransparency="true"
                                    noresize=""
                                    role="button"
                                    class="vidplayer_img"
                                    style="border:none;margin-bottom: -1rem;
                                        background: url({{$thumbnailUrl}});
                                        background-position: center;
                                        background-size: cover;
                                        background-repeat: no-repeat;"
                                >
                                </iframe>
                                @if ($performanceStatusNow === 'live')
                                    <!-- <div class="live_badge"></div> -->
                                @endif
                                <div class="vidplayer_banner">
                                    <h2>{{$performerName}}</h2>
                                    <p><?php echo nl2br($description); ?></p>
                                    @if ($performanceStatusNow === 'vod')
                                        <p class="vidplayer_deadline vod txt-bold">FINISHED AT: {{$expiredDate}}</p>
                                    @elseif ($performanceStatusNow === 'live')
                                        <p class="vidplayer_deadline live txt-bold">{{$liveDate}}</p>
                                    @endif
                                </div>
                            </section>
                            <section>
                                @foreach ($thisWeekPerformancesDateGroup as $key => $val)
                                    <?php
                                        $weekDate = strtoupper(date("j M Y", strtotime($key)));
                                    ?>
                                    <section class="vidplayer_listwrapper">
                                        <h3 class="vidplayer_schtitle">{{$weekDate}}</h3>
                                        @foreach ($val as $v)
                                            <?php
                                            $id = $v->id;
                                            $titleSlug = $v->title_slug;
                                            $performanceStatusNow = $v->performance_status_now;
                                            if ($currentPlayedPerformance->id === $id && $performanceStatusNow !== 'live') {
                                                $performanceStatusNow = 'now_playing';
                                            }
                                            $thumbnailUrl = $v->thumbnail_url;
                                            if (!$thumbnailUrl) {
                                                $thumbnailUrl = $staticAssetPrefix. '/static/icons/soonavailable.png';
                                            }
                                            $performerName = strtoupper($v->performer_name);
                                            $startLiveTime = explode(':', explode(' ', $v->start_live)[1]);
                                            $startLiveTime = $startLiveTime[0] . '.' . $startLiveTime[1];
                                            $endLiveTime = explode(':', explode(' ', $v->end_live)[1]);
                                            $endLiveTime = $endLiveTime[0] . '.' . $endLiveTime[1];
                                            $startLiveDate = explode(' ', $v->start_live)[0];
                                            $startLiveDate = date("j M Y", strtotime($startLiveDate));
                                            $expiredDate = explode(' ', $v->expired_date)[0];
                                            $expiredDate = strtoupper(date("j M Y", strtotime($expiredDate)));
                                            //$liveDate = strtoupper($startLiveTime . ' - ' . $endLiveTime . ' / ' . $startLiveDate);
                                            $title = $v->title;
                                            $dateTracker = explode(' ', $v->start_live)[0];
                                            $dateTracker = date("l / d F Y", strtotime($dateTracker));

                                            // revision
                                            $sDateSplit = explode('-', explode(' ', $v->start_live)[0]);
                                            $eDateSplit = explode('-', explode(' ', $v->expired_date)[0]);
                                            if ($sDateSplit[1] === $eDateSplit[1]) {
                                                if ($sDateSplit[2] === $eDateSplit[2]) {
                                                    $liveDate = date("j M Y", strtotime(explode(' ', $v->start_live)[0]));
                                                } else {
                                                    $liveDate = date("j", strtotime(explode(' ', $v->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $v->expired_date)[0]));
                                                }
                                            } else {
                                                $liveDate = date("j M Y", strtotime(explode(' ', $v->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $v->expired_date)[0]));
                                            }
                                            $liveDate = strtoupper($liveDate);
                                            ?>
                                            @switch($performanceStatusNow)
                                                @case('live')
                                                    <div
                                                        class="vidplayer_schedule"
                                                    >
                                                        <div class="trapezoid live">
                                                            <a
                                                                href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                                onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                                class="list_text"
                                                            >
                                                                <h3>{{$performerName}}</h3>
                                                                <p>{{$liveDate}}</p>
                                                            </a>
                                                        </div>
                                                        <figure class="schedule_icon"></figure>
                                                    </div>
                                                @break
                                                @case('coming_soon')
                                                    <div class="vidplayer_schedule">
                                                        <div class="trapezoid soon">
                                                            <div
                                                                class="trapezoid_popup"
                                                                style="background: url({{$staticAssetPrefix}}/static/icons/soonavailable.png);
                                                                    background-position: center;
                                                                    background-repeat: no-repeat;
                                                                    background-size: cover;
                                                                    height: 100%;">
                                                                <h3 class="trapezoid_popup_text">WILL BE AVAILABLE SOON</h3>
                                                                <p class="trapezoid_popup_text">{{$liveDate}}</p>
                                                            </div>
                                                            <div class="list_text">
                                                                <h3>{{$performerName}}</h3>
                                                                <p>{{$liveDate}}</p>
                                                            </div>
                                                        </div>
                                                        <figure class="schedule_icon"></figure>
                                                    </div>
                                                @break
                                                @case('vod')
                                                    <div
                                                        class="vidplayer_schedule"
                                                    >
                                                        <div class="trapezoid vod">
                                                            <a
                                                                href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                                onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                                class="list_text"
                                                            >
                                                                <h3>{{$performerName}}</h3>
                                                                <p>FINISHED AT: {{$expiredDate}}</p>
                                                            </a>
                                                        </div>
                                                        <figure class="schedule_icon"></figure>
                                                    </div>
                                                @break
                                                @case('now_playing')
                                                    <div
                                                        class="vidplayer_schedule"
                                                    >
                                                        <div class="trapezoid playing">
                                                            <a
                                                                href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                                onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                                class="list_text"
                                                            >
                                                                <h3>{{$performerName}}</h3>
                                                                <p>FINISHED AT: {{$expiredDate}}</p>
                                                            </a>
                                                        </div>
                                                        <figure class="schedule_icon"></figure>
                                                    </div>
                                                @break
                                                @case('expired')
                                                    <div class="vidplayer_schedule">
                                                        <div class="trapezoid expired">
                                                            <div class="list_text">
                                                                <h3>{{$performerName}}</h3>
                                                                <p>SHOW FINISHED</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @break
                                            @endswitch
                                        @endforeach
                                    </section>
                                @endforeach
                            </section>
                        </main>
                    </section>
                </section>


            <!-- If list performance -->
            @else
                <section id="landing_performance">
                    <section class="panels">
                        <header>
                            <h3 class="section_title text_xl">WATCH PERFORMANCE</h3>
                        </header>
                        <main id="highlight_banner">
                            @if ($featuredPerformance)
                                <?php
                                    $id = $featuredPerformance->id;
                                    $titleSlug = $featuredPerformance->title_slug;
                                    $type = $featuredPerformance->type;
                                    $performanceStatusNow = $featuredPerformance->performance_status_now;
                                    $thumbnailUrl = $featuredPerformance->thumbnail_url;
                                    if (!$thumbnailUrl) {
                                        $thumbnailUrl = $staticAssetPrefix. '/static/icons/placeholder.svg';
                                    }
                                    $performerName = strtoupper($featuredPerformance->performer_name);
                                    $startLiveTime = explode(':', explode(' ', $featuredPerformance->start_live)[1]);
                                    $startLiveTime = $startLiveTime[0] . '.' . $startLiveTime[1];
                                    $endLiveTime = explode(':', explode(' ', $featuredPerformance->end_live)[1]);
                                    $endLiveTime = $endLiveTime[0] . '.' . $endLiveTime[1];
                                    $startLiveDate = explode(' ', $featuredPerformance->start_live)[0];
                                    $startLiveDate = date("l, j M Y", strtotime($startLiveDate));
                                    //$liveDate = strtoupper(
                                    //    $performerName . ' / ' . $startLiveDate . ' / ' . $startLiveTime . ' - ' . $endLiveTime
                                    //);
                                    $liveDate = strtoupper(
                                        $performerName . ' / '
                                    );
                                    $title = $featuredPerformance->title;
                                    $dateTracker = explode(' ', $featuredPerformance->start_live)[0];
                                    $dateTracker = date("l / d F Y", strtotime($dateTracker));

                                    // revision
                                    $sDateSplit = explode('-', explode(' ', $featuredPerformance->start_live)[0]);
                                    $eDateSplit = explode('-', explode(' ', $featuredPerformance->expired_date)[0]);
                                    if ($sDateSplit[1] === $eDateSplit[1]) {
                                        if ($sDateSplit[2] === $eDateSplit[2]) {
                                            $liveDate = $liveDate . date("j M Y", strtotime(explode(' ', $featuredPerformance->start_live)[0]));
                                        } else {
                                            $liveDate = $liveDate . date("j", strtotime(explode(' ', $featuredPerformance->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $featuredPerformance->expired_date)[0]));
                                        }
                                    } else {
                                        $liveDate = $liveDate . date("j M Y", strtotime(explode(' ', $featuredPerformance->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $featuredPerformance->expired_date)[0]));
                                    }
                                    $liveDate = strtoupper($liveDate);
                                ?>
                                @switch($performanceStatusNow)
                                    @case('live')
                                    <figure
                                        onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');redirect('performance', '/soundrenaline/performance/{{$id}}/{{$titleSlug}}')"
                                        role="button"
                                        class="highlight_img"
                                        style="background: url({{$thumbnailUrl}});
                                            background-position: center;
                                            background-size: cover;
                                            background-repeat: no-repeat;"
                                    ></figure>
                                    @if ($type == 'special')
                                        <div class="live_badge"></div>
                                    @endif
                                    @break
                                    @case('coming_soon')
                                    <figure
                                        role="button"
                                        class="highlight_img"
                                        style="background: url({{$thumbnailUrl}});
                                          background-position: center;
                                          background-size: cover;
                                          background-repeat: no-repeat;"
                                    ></figure>
                                    <div class="highlight_overlay overlay_icon">
                                        <label>SOON</label>
                                        <span>{{$performerName}}</span>
                                    </div>
                                    <div class="highlight_showtime">
                                        <p>
                                            {{$liveDate}}
                                        </p>
                                    </div>
                                    @break
                                    @case('expired')
                                    <figure
                                        role="button"
                                        class="highlight_img"
                                        style="background: url({{$thumbnailUrl}});
                                            background-position: center;
                                            background-size: cover;
                                            background-repeat: no-repeat;"
                                    ></figure>
                                    @break
                                    @default
                                    <figure
                                        onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');redirect('performance', '/soundrenaline/performance/{{$id}}/{{$titleSlug}}')"
                                        role="button"
                                        class="highlight_img"
                                        style="background: url({{$thumbnailUrl}});
                                            background-position: center;
                                            background-size: cover;
                                            background-repeat: no-repeat;"
                                    ></figure>
                                    @break
                                @endswitch
                            @endif
                        </main>
                    </section>
                    <section class="panels">
                        <header>
                            <h2 class="section_title text_xl">OUR SPECIAL PERFORMANCES</h2>
                        </header>
                        <main id="special_performances" class="special_performance">
                            @if ($specialPerformances && count($specialPerformances) > 0)
                                @foreach ($specialPerformances as $val)
                                    <?php
                                        $id = $val->id;
                                        $titleSlug = $val->title_slug;
                                        $performanceStatusNow = $val->performance_status_now;
                                        $thumbnailUrl = $val->thumbnail_url;
                                        if (!$thumbnailUrl) {
                                            $thumbnailUrl = $staticAssetPrefix. '/static/icons/placeholder.svg';
                                        }
                                        $performerName = strtoupper($val->performer_name);
                                        $startLiveTime = explode(':', explode(' ', $val->start_live)[1]);
                                        $startLiveTime = $startLiveTime[0] . '.' . $startLiveTime[1];
                                        $endLiveTime = explode(':', explode(' ', $val->end_live)[1]);
                                        $endLiveTime = $endLiveTime[0] . '.' . $endLiveTime[1];
                                        $startLiveDate = explode(' ', $val->start_live)[0];
                                        $startLiveDate = date("j M Y", strtotime($startLiveDate));
                                        //$comingSoonDate = strtoupper($startLiveTime . ' - ' . $endLiveTime . ' / ' . $startLiveDate);
                                        $title = $val->title;
                                        $dateTracker = explode(' ', $val->start_live)[0];
                                        $dateTracker = date("l / d F Y", strtotime($dateTracker));

                                        // revision
                                        $sDateSplit = explode('-', explode(' ', $val->start_live)[0]);
                                        $eDateSplit = explode('-', explode(' ', $val->expired_date)[0]);
                                        if ($sDateSplit[1] === $eDateSplit[1]) {
                                            if ($sDateSplit[2] === $eDateSplit[2]) {
                                                $comingSoonDate = date("j M Y", strtotime(explode(' ', $val->start_live)[0]));
                                            } else {
                                                $comingSoonDate = date("j", strtotime(explode(' ', $val->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $val->expired_date)[0]));
                                            }
                                        } else {
                                            $comingSoonDate = date("j M Y", strtotime(explode(' ', $val->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $val->expired_date)[0]));
                                        }
                                        $comingSoonDate = strtoupper($comingSoonDate);
                                    ?>
                                    @switch($performanceStatusNow)
                                        @case('live')
                                            <figure class="sp_card">
                                                <div class="live_badge"></div>
                                                <a
                                                    href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                    onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                    class="card_img"
                                                    style="background: url({{$thumbnailUrl}});
                                                    background-position: center;
                                                    background-size: cover;
                                                    background-repeat: no-repeat;"
                                                ></a>
                                            </figure>
                                        @break
                                        @case('coming_soon')
                                            <figure class="sp_card">
                                                <div
                                                    role="button"
                                                    class="card_img"
                                                    style="background: url({{$thumbnailUrl}});
                                                    background-position: center;
                                                    background-size: cover;
                                                    background-repeat: no-repeat;"
                                                ></div>
                                                <div class="card_overlay notstarted overlay_icon">
                                                    <h3 class="card_label">{{$performerName}}</h3>
                                                    <div class="card_popup">
                                                        <h3>WILL BE AVAILABLE SOON</h3>
                                                        <p>{{$comingSoonDate}}</p>
                                                    </div>
                                                </div>
                                            </figure>
                                        @break
                                        @case('vod')
                                            <figure class="sp_card">
                                                <div
                                                    role="button"
                                                    class="card_img"
                                                    style="background: url({{$thumbnailUrl}});
                                                    background-position: center;
                                                    background-size: cover;
                                                    background-repeat: no-repeat;"
                                                ></div>
                                                <a
                                                    href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                    onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                    class="card_overlay overlay_icon"
                                                >
                                                    <div class="card_icon icon_vod"></div>
                                                </a>
                                            </figure>
                                        @break
                                        @case('expired')
                                            <figure class="sp_card">
                                                <div
                                                    onclick=""
                                                    role="button"
                                                    class="card_img"
                                                    style="background: url({{$thumbnailUrl}});
                                                    background-position: center;
                                                    background-size: cover;
                                                    background-repeat: no-repeat;"
                                                ></div>
                                                <div class="card_overlay inaccessible"></div>
                                            </figure>
                                        @break
                                    @endswitch
                                @endforeach
                            @endif
                        </main>
                    </section>
                    <section class="panels">
                        <header>
                            <h3 class="section_title text_xl">THIS WEEK PERFORMANCES</h3>
                        </header>
                        <main id="now_performances">
                            @if ($thisWeekPerformancesDateGroup && count($thisWeekPerformancesDateGroup) > 0)
                                @foreach ($thisWeekPerformancesDateGroup as $key => $val)
                                    <?php
                                        $weekDate = strtoupper(date("l - j F Y", strtotime($key)));
                                    ?>
                                    <section>
                                        <p class="subsection_title txt-bold">{{$weekDate}}</p>
                                        <div class="ordinary_performances_wrapper">
                                            @foreach ($val as $v)
                                                <?php
                                                    $id = $v->id;
                                                    $titleSlug = $v->title_slug;
                                                    $performanceStatusNow = $v->performance_status_now;
                                                    $type = $v->type;
                                                    $thumbnailUrl = $v->thumbnail_url;
                                                    if (!$thumbnailUrl) {
                                                        $thumbnailUrl = $staticAssetPrefix. '/static/icons/placeholder.svg';
                                                    }
                                                    $performerName = strtoupper($v->performer_name);
                                                    $startLiveTime = explode(':', explode(' ', $v->start_live)[1]);
                                                    $startLiveTime = $startLiveTime[0] . '.' . $startLiveTime[1];
                                                    $endLiveTime = explode(':', explode(' ', $v->end_live)[1]);
                                                    $endLiveTime = $endLiveTime[0] . '.' . $endLiveTime[1];
                                                    $startLiveDate = explode(' ', $v->start_live)[0];
                                                    $startLiveDate = date("j M Y", strtotime($startLiveDate));
                                                    $expiredDate = explode(' ', $v->expired_date)[0];
                                                    $expiredDate = date("j M Y", strtotime($expiredDate));
                                                    //$comingSoonDate = strtoupper($startLiveTime . ' - ' . $endLiveTime . ' / ' . $startLiveDate);
                                                    //$liveTime = strtoupper($startLiveTime . ' - ' . $endLiveTime);
                                                    $title = $v->title;
                                                    $dateTracker = explode(' ', $v->start_live)[0];
                                                    $dateTracker = date("l / d F Y", strtotime($dateTracker));

                                                    // revision
                                                    $sDateSplit = explode('-', explode(' ', $v->start_live)[0]);
                                                    $eDateSplit = explode('-', explode(' ', $v->expired_date)[0]);
                                                    if ($sDateSplit[1] === $eDateSplit[1]) {
                                                        if ($sDateSplit[2] === $eDateSplit[2]) {
                                                            $liveTime = date("j M Y", strtotime(explode(' ', $v->start_live)[0]));
                                                        } else {
                                                            $liveTime = date("j", strtotime(explode(' ', $v->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $v->expired_date)[0]));
                                                        }
                                                    } else {
                                                        $liveTime = date("j M Y", strtotime(explode(' ', $v->start_live)[0])).' - '.date("j M Y", strtotime(explode(' ', $v->expired_date)[0]));
                                                    }
                                                    $liveTime = strtoupper($liveTime);
                                                    $comingSoonDate = $liveTime;
                                                ?>
                                                @switch($performanceStatusNow)
                                                    @case('live')
                                                        <div class="ordinary_performances">
                                                            <figure class="card tertiarysize">
                                                                <a
                                                                    href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                                    onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                                    role="button"
                                                                    class="card_img"
                                                                    style="background: url({{$thumbnailUrl}});
                                                                        background-position: center;
                                                                        background-size: cover;
                                                                        background-repeat: no-repeat;"
                                                                ></a>
                                                                @if ($type == 'special')
                                                                    <div class="live_badge"></div>
                                                                @endif
                                                            </figure>
                                                            <article>
                                                                <p>{{$performerName}}</p>
                                                                <p>{{$liveTime}}</p>
                                                            </article>
                                                        </div>
                                                    @break
                                                    @case('coming_soon')
                                                        <div class="ordinary_performances">
                                                            <figure class="card tertiarysize">
                                                                <div
                                                                    role="button"
                                                                    class="card_img"
                                                                    style="background: url({{$thumbnailUrl}});
                                                                        background-position: center;
                                                                        background-size: cover;
                                                                        background-repeat: no-repeat;"
                                                                ></div>
                                                                <div class="card_overlay notstarted overlay_icon">
                                                                    <div class="card_popup">
                                                                        <h3>WILL BE AVAILABLE SOON</h3>
                                                                        <p>{{$comingSoonDate}}</p>
                                                                    </div>
                                                                </div>
                                                            </figure>
                                                            <article>
                                                                <p>{{$performerName}}</p>
                                                                <p>{{$liveTime}}</p>
                                                            </article>
                                                        </div>
                                                    @break
                                                    @case('vod')
                                                        <div class="ordinary_performances">
                                                            <figure class="card tertiarysize">
                                                                <div
                                                                    role="button"
                                                                    class="card_img"
                                                                    style="background: url({{$thumbnailUrl}});
                                                                        background-position: center;
                                                                        background-size: cover;
                                                                        background-repeat: no-repeat;"
                                                                ></div>
                                                                <a
                                                                    href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                                    onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                                    class="card_overlay overlay_icon"
                                                                >
                                                                    <div class="card_icon icon_vod"></div>
                                                                </a>
                                                            </figure>
                                                            <article>
                                                                <p>{{$performerName}}</p>
                                                                <p>FINISHED AT:</p>
                                                                <p>{{$expiredDate}}</p>
                                                            </article>
                                                        </div>
                                                    @break
                                                    @case('expired')
                                                        <div class="ordinary_performances">
                                                            <figure class="card tertiarysize">
                                                                <div
                                                                    role="button"
                                                                    class="card_img"
                                                                    style="background: url({{$thumbnailUrl}});
                                                                        background-position: center;
                                                                        background-size: cover;
                                                                        background-repeat: no-repeat;"
                                                                ></div>
                                                                <div class="card_overlay inaccessible"></div>
                                                            </figure>
                                                            <article>
                                                                <p>{{$performerName}}</p>
                                                                <p>SHOW FINISHED</p>
                                                            </article>
                                                        </div>
                                                    @break
                                                @endswitch
                                            @endforeach
                                        </div>
                                    </section>
                                @endforeach
                            @endif
                        </main>
                    </section>
                </section>
            @endif


            <section class="panels">
                <article class="creators_park_banner">
                    <p class="creators_park_text">
                        GET THE FULL SOUNDRENALINE EXPERIENCE ONLY AT CREATORS PARK
                    </p>
                    <a
                        href="/soundrenaline/creators-park"
                        onclick="tracker('special_event_interaction', 'click_redirect', 'redirect_to_creators_park');"
                        class="link_btn"
                    >
                        @if ($isLive)
                            <h2 class="text_lg">
                                GO TO CREATORS PARK
                            </h2>
                        @else
                            <h2 class="text_lg">
                                CHECK MORE DETAILS
                            </h2>
                        @endif
                    </a>
                </article>
            </section>
        </main>
        <figure
            class="pattern_img_desktop"
            style="position: absolute;
            right: 0;
            top: 0;
            background-image: url({{$staticAssetPrefix}}/static/images/rt_pattern_desktop.svg);
            background-position: top;
            background-size: cover;
            background-repeat: no-repeat;
            width: 275px;
            height: 200px;">
        </figure>

        <figure
            class="pattern_img_desktop"
            style="position: absolute;
            right: 0;
            bottom: 1rem;
            background-image: url({{$staticAssetPrefix}}/static/images/rb_pattern_desktop.svg);
            background-position: top;
            background-size: cover;
            background-repeat: no-repeat;
            width: 275px;
            height: 200px;">
        </figure>

        <figure
            class="pattern_img_desktop"
            style="position: absolute;
            left: 0;
            bottom: 0;
            background-image: url({{$staticAssetPrefix}}/static/images/lb_pattern_desktop.svg);
            background-position: top;
            background-size: cover;
            background-repeat: no-repeat;
            width: 600px;
            height: 400px;">
        </figure>
        <footer>
            <section>


                <!-- Cookie consent -->
                <div id="cookie_consent">
                    <div class="cookie_wrapper">
                        <div class="cookie_child">
                            <img
                                src="{{$staticAssetPrefix}}/static/icons/cookie_consent.png"
                                alt="cookie consent"
                                width="auto"
                                height="auto" />
                            <p>This site use cookies to optimise site functionality and give you the best possible experience.</p>
                        </div>
                        <div class="cookie_child">
                            <button
                                onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-out');setDwUserConsent('refuse');"
                                class="cookie_btn unconfirmed">
                                REFUSE
                            </button>
                            <button
                                onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-in');setDwUserConsent('accept');"
                                class="cookie_btn confirmed">
                                ACCEPT
                            </button>
                        </div>
                        <img
                            role="button"
                            onclick="closeCookieConsent()"
                            class="close_cookie"
                            src="{{$staticAssetPrefix}}/static/icons/close_cookie.png"
                            alt="close cookie"
                            width="10"
                            height="10"  />
                    </div>
                </div>
                <script>
                    if (checkDwCookie("dw_user_consent") !== "") {
                        document.getElementById("cookie_consent").style.display = "none";
                    }
                </script>


                <div id="smoking_warning">
                    <img src="{{$staticAssetPrefix}}/static/icons/thorax.png" width="auto" height="auto" alt="smoking impact" />
                    <p style="font-family: 'Inter', sans-serif;">
                        PERINGATAN: <br />
                        KARENA MEROKOK, SAYA TERKENA KANKER TENGGOROKAN, LAYANAN BERHENTI MEROKOK (0800-177-6565)
                    </p>
                    <img src="{{$staticAssetPrefix}}/static/icons/18plus.png" width="auto" height="auto" alt="18 plus" />
                </div>
            </section>
        </footer>
    </body>
</html>
