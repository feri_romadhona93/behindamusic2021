<!DOCTYPE html>
<html>
    <head>


        <!-- Global site tag (gtag.js) - Google Analytics GA Classic Pillar Video -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$gaClassicVideoId}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$gaClassicVideoId}}');
        </script>


        <!-- Global site tag (gtag.js) - Google Analytics GA 4 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$ga4Id}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$ga4Id}}');
        </script>


        <!-- Google Tag Manager -->
        <script>
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'pillar' : 'video'
            });
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth={{$gtmAuth}}'+
                '&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','{{$gtmId}}');
        </script>


        <!-- Google Tag Manager - DMP -->
        <script>
            window.dataLayerDMP = window.dataLayerDMP || [];
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore( j,f);
            })(window,document,'script','dataLayerDMP','{{$gtmDmpId}}');
        </script>


        <!-- comScore -->
        <script>
            let _comscore = [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
                let s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>


        <!-- Alexa Certify -->
        <script type="text/javascript">
            _atrk_opts = { atrk_acct:"8oNJt1FYxz20cv", domain:"rctiplus.com",dynamic: true};
            (function() { let as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; let s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>


        <!-- Tracker function -->
        <script>
            function tracker (event_category, event_action, event_label) {
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                });
            }
            window.tracker = tracker;
            function checkAgeSubmission() {
                function hasClass(element, className) {
                    return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
                }
                if (hasClass(document.getElementById('modalSoftAgeGateError'), 'hide')) {
                    tracker('special_event_interaction', 'age_confirmation', 'successful_submission');
                } else {
                    tracker('special_event_interaction', 'age_confirmation', 'failed_submission ');
                }
            }
        </script>


        <!-- Tracker function - DMP -->
        <script>
            function trackerDMPCookie (cookieValue = 'opt-out') {
                window.dataLayerDMP.push({
                    'event': 'cookieSettings',
                    'cookieType': 'advertising',
                    'cookieValue': cookieValue
                });
            }
            function checkDwCookie(name) {
                let ca = document.cookie.split(';');
                for(let i = 0; i < ca.length; i++) {
                    let c = ca[i];
                    while (c.charAt(0) === ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) === 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }
            function setDwAdvertisementCookie(triggerTracker = false, value = 'opt-out', exdays = 365, init = false) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                if (checkDwCookie("dw_Advertisement_cookie") === "") {
                    document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                    if (triggerTracker === true) {
                        trackerDMPCookie(value);
                    }
                } else {
                    if (init === false) {
                        document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                        if (triggerTracker === true) {
                            trackerDMPCookie(value);
                        }
                    }
                }
            }
            function setDwUserConsent(value = 'false', exdays = 365) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                document.cookie = 'dw_user_consent=' + value + ";" + expires + ";path=/soundrenaline";
            }
            setDwAdvertisementCookie(false, 'opt-out', 365, true);
            window.trackerDMPCookie = trackerDMPCookie;
        </script>


        <!-- Meta tags -->
        <meta name="viewport" content="width = device-width, initial-scale =1.0, maximum-scale = 1.0, minimum-scale = 1.0, user-scalable = 0, minimal-ui" />
        <meta name="format-detection" content="telephone=no">
        <link href="{{$staticAssetPrefix}}/static/unbranded/css/main.css?v={{$assetVersion}}" rel="stylesheet" />
        <script type="application/javascript" src="{{$staticAssetPrefix}}/static/unbranded/js/libs.min.js?v={{$assetVersion}}"></script>


        <?php
        $metaTitle = "Soundrenaline 2021";
        $metaDesc = "Get Ready For A New Experience In 2021";
        $metaKeywords = null;
        $metaImage = null;
        $metaImageAlt = null;
        $metaUploadDate = "2021-09-15T08:00:00+07:00";
        $metaDuration = "PT12H";
        if ($meta) {
            $metaTitle = $meta->meta_title;
            $metaDesc = $meta->meta_description;
            $metaKeywords = $meta->meta_keywords;
            $metaImage = $meta->meta_image_url;
            $metaImageAlt = $meta->meta_image_alt;
        }
        ?>


        <!-- SEO meta tags -->
        <title>{{$metaTitle}}</title>
        <link rel="shortcut icon" type="image/png" href="/assets/image/elements/favicon.png">
        <meta name="title" content="{{$metaTitle}}">
        <meta name="description" content="{{$metaDesc}}">
        <meta name="keywords" content="{{$metaKeywords}}">
        <meta property="og:title" content="{{$metaTitle}}">
        <meta property="og:description" content="{{$metaDesc}}">
        <meta property="og:type" content="website">
        <meta property="og:url" content="{{$pageUrl}}">
        <meta property="og:image" content="{{$metaImage}}">
        <meta property="fb:app_id" content="211272363627736">
        <meta property="og:site_name" content="RCTI+">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@RCTIPlus">
        <meta name="twitter:creator" content="@RCTIPlus">
        <meta name="twitter:title" content="{{$metaTitle}}">
        <meta name="twitter:description" content="{{$metaDesc}}">
        <meta name="twitter:image" content="{{$metaImage}}">
        <meta name="twitter:image:alt" content="{{$metaImageAlt}}">
        <meta name="twitter:url" content="{{$pageUrl}}">
        <meta name="twitter:domain" content="{{$pageUrl}}">
        <meta name="robots" content="index, follow"/>
        <link rel="canonical" href="{{$hostname}}/soundrenaline"/>


        <!-- JSON-LD -->
        <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "Event",
                "name": "Nonton Festival Musik Terbesar 2021. The Sound is Back!",
                "startDate": "2021-09-15T08:00:00+07:00",
                "description": "The Sound Is Back! Nikmati festival musik terbesar 2021 bersama secara virtual. Klik untuk melihat jadwal dan lineup festival musik 2021 terbaru di RCTI+.",
                "image": "https://static.rctiplus.id/media/900/files/fta_rcti/soundrenaline/soundrenaline_c60735b1b6.jpg",
                "eventStatus": "https://schema.org/EventMovedOnline",
                "eventAttendanceMode": "https://schema.org/OnlineEventAttendanceMode",
                "location": {
                    "@type": "VirtualLocation",
                    "url": "https://rctiplus.com/soundrenaline/creators-park"
                },
                "organizer": {
                    "@type": "Organization",
                    "name": "Soundrenaline",
                    "sameAs": [
                        "https://soundrenaline.id/",
                        "https://g.co/kgs/m35riY",
                        "https://www.instagram.com/soundrenaline.co.id",
                        "https://twitter.com/SOUNDRENALlNE"
                    ]
                },
                "aggregateRating": {
                    "@type": "http://schema.org/AggregateRating",
                    "worstRating": 1,
                    "bestRating": 5,
                    "ratingValue": 5.0,
                    "reviewCount": 412
                }
            }
        </script>


        <!-- Enter the event -->
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script>
            function enterTheEvent() {
                axios({
                    method: "post",
                    url: "/soundrenaline/enter/event",
                }).then(function(){
                    window.location.href= '/soundrenaline';
                });
            }
        </script>


        <style>
            body {
                margin: 0;
                padding: 0;
                font-family: "Sora", "Helvetica", "Arial", sans-serif;
                color: #000;
                font-size: 14px;
                background-image: url({{$staticAssetPrefix}}/static/unbranded/assets/images/bg.png);
                background-size: cover;
            }
            .gitar {
                pointer-events: none;
                width: 720px;
                height: 720px;
                background: url({{$staticAssetPrefix}}/static/unbranded/assets/images/guitar720.png) left top;
                background-position: 0px top;
                -webkit-animation: gitaranimasi 2.5s steps(24) infinite alternate;
                animation: gitaranimasi 2.5s steps(24) infinite alternate;
                position: absolute;
                left: 0;
                top: 0;
                transform: translateY(-52%) translateX(-29%);
            }
            .modal__content > .inner {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                background-image: url({{$staticAssetPrefix}}/static/unbranded/assets/images/popup-desktop.png);
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
                padding: 20px 25px 25px 20px;
                display: flex;
                align-items: center;
                justify-content: center;
                text-align: center;
            }
            .softagegate__form label select {
                width: 100%;
                padding: 10px;
                border: 1px solid #fff;
                color: #0b0d10;
                outline: none;
                font-size: 1rem;
                font-family: "Sora", "Helvetica", "Arial", sans-serif;
                -moz-appearance: none;
                -webkit-appearance: none;
                appearance: none;
                position: relative;
                background-image: url({{$staticAssetPrefix}}/static/unbranded/assets/images/arrow-down.svg);
                background-repeat: no-repeat;
                background-size: 16px;
                background-position: right 10px center;
                background-color: #fff;
            }
            @media only screen and (max-width: 425px) {
                .modal__content > .inner {
                    background-image: url({{$staticAssetPrefix}}/static/unbranded/assets/images/popup-mobile.png);
                }
            }
        </style>
    </head>
    <body onload="checkCookie()">
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmId}}&gtm_auth={{$gtmAuth}}&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmDmpId}}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1">
        </noscript>
        <noscript>
            <img src="https://certify.alexametrics.com/atrk.gif?account=8oNJt1FYxz20cv" style="display:none" height="1" width="1" alt="" />
        </noscript>


        <!-- MODAL -->
        <!-- mdl welcome -->
        <div data-nosnippet class="modal modal--center hide" id="modalSoftAgeGate">
            <div data-nosnippet class="modal__content">
                <div data-nosnippet class="inner">
                    <div data-nosnippet class="softagegate">
                        <h3><span data-nosnippet>Welcome</span></h3>
                        <p><span data-nosnippet>Informasi dalam website ini ditujukan untuk perokok berusia 18 tahun atau lebih dan tinggal di wilayah Indonesia.</span></p>
                        <p class="mod"><span data-nosnippet><strong>Isi tanggal lahir dulu ya.</strong></span></p>
                        <form  name="age_form">
                            <div data-nosnippet class="softagegate__form">
                                <label for="bulan">
                                    <select name="bulan" id="month">
                                        <option value="0" disabled selected>Bulan</option>
                                    </select>
                                </label>
                                <label for="tahun">
                                    <select name="tahun" id="year">
                                        <option value="0" disabled selected>Tahun</option>
                                    </select>
                                </label>
                            </div>
                            <button class="softagegate__button" type="button" onClick="checkAge();checkAgeSubmission();">Konfirmasi</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div data-nosnippet class="modal modal--center hide" id="modalSoftAgeGateError">
            <div data-nosnippet class="modal__content">
                <div data-nosnippet class="inner">
                    <div data-nosnippet class="softagegate">
                        <p><span data-nosnippet>Sesuai peraturan yang berlaku, website ini hanya bisa diakses oleh pengunjung yang berumur 18 tahun ke atas.</span></p>
                        <button class="softagegate__button" type="button" onClick="back()">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
        <div data-nosnippet class="backdrop hide" id="backdrop">&nbsp;</div>
        <!-- !MODAL -->


        <!-- PRELOADER -->
        <div data-nosnippet class="preloader" id="loading">
            <div data-nosnippet class="preloader__content">
                <div data-nosnippet class="block-image">
                    <img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/rctiplus.png" alt="Logo" />
                </div>
                <p><span data-nosnippet>Loading, please wait...</span></p>
                <div data-nosnippet class="loading">
                    <div data-nosnippet class="loader">
                        <div data-nosnippet class="trackbar">
                            <div data-nosnippet class="loadbar"></div>
                        </div>
                    </div>
                    <div data-nosnippet class="percentage" id="persen"></div>
                </div>
            </div>
        </div>
        <!-- !PRELOADER -->


        <div data-nosnippet class="ornament ornament-1 rellax" data-rellax-speed="-5"><div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/ornament-left.png" alt="Soundrenaline"></div></div>
        <div data-nosnippet class="ornament ornament-2 rellax" data-rellax-speed="-5.8"><div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/ornament-right.png" alt="Soundrenaline"></div></div>
        <main>
            <div data-nosnippet class="awan awan--desktop">
                <div data-nosnippet class="awan-atas rellax" data-rellax-speed="-5">
                    <div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/awanAtasDesktop.png" alt="Soundrenaline"></div>
                </div>
                <div data-nosnippet class="awan-bawah rellax" data-rellax-speed="-1">
                    <div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/awanBawahDesktop.png" alt="Soundrenaline"></div>
                </div>
            </div>
            <div data-nosnippet class="awan awan--mobile">
                <div data-nosnippet class="awan-atas rellax" data-rellax-speed="-2">
                    <div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/awanAtasMobile.png" alt="Soundrenaline"></div>
                </div>
                <div data-nosnippet class="awan-bawah rellax" data-rellax-speed="1">
                    <div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/awanBawahMobile.png" alt="Soundrenaline"></div>
                </div>
                <div data-nosnippet class="awan-bawah rellax" data-rellax-speed="3">
                    <div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/awanBawah2Mobile.png" alt="Soundrenaline"></div>
                </div>
            </div>
            <div data-nosnippet class="hero">
                <div data-nosnippet class="hero__bg">
                    <div data-nosnippet class="block-image hero__bg-desktop"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/hero desktop.png" alt=""></div>
                    <div data-nosnippet class="block-image hero__bg-mobile"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/hero mobile.png" alt=""></div>
                    <div data-nosnippet class="hero__headline rellax" data-rellax-speed="-1.4">
                        <h1 class="text-style"><span data-nosnippet>TERIMA KASIH</span></h1>
                        <h1 class="mod text-style"><span data-nosnippet>FESTIVAL MUSIK TERBESAR SEBULAN PENUH</span></h1>
                    </div>
                    <div data-nosnippet class="hero__ball heroBallViewer"></div>
                </div>
            </div>
            <section data-nosnippet class="desc desc--mod">
                <div data-nosnippet class="octa octa-1"><div data-nosnippet class="octa__wrapper"><div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/ornament-octa-1.png" alt="Soundrenaline"></div></div></div>
                <div data-nosnippet class="octa octa-2"><div data-nosnippet class="octa__wrapper"><div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/ornament-octa-2.png" alt="Soundrenaline"></div></div></div>
                <div data-nosnippet class="octa octa-3"><div data-nosnippet class="octa__wrapper"><div data-nosnippet class="block-image"><img src="{{$staticAssetPrefix}}/static/unbranded/assets/images/ornament-octa-3.png" alt="Soundrenaline"></div></div></div>
                <div data-nosnippet class="ball-scroll-trigger" id="ballScrollTrigger"></div>
                <div data-nosnippet class="ball-scroll ballScrollViewer"></div>
                <div data-nosnippet class="container">
                    <div data-nosnippet class="desc__gambar rellax" data-rellax-speed="-0.5">
                        <div data-nosnippet class="gitar"></div>
                    </div>
                    <div data-nosnippet class="desc__text">
                        <p><span data-nosnippet>Setelah 2 tahun hiatus, festival musik yang pastinya sudah lo tunggu-tunggu kembali lagi. Dalam edisi spesial tahun ini, festival musik ini akan eksklusif dibuka untuk seluruh rakyat Indonesia.</span></p>
                        <p><span data-nosnippet>&nbsp;</span></p>
                        <p><span data-nosnippet>Dengan konsep yang baru, lo bisa menikmati berbagai macam hiburan langsung dari mana saja. Mulai dari musik, art, fashion hingga komedi, semua bisa lo temukan di acara&nbsp;ini.</span></p>
                    </div>
                </div>
            </section>
            <section data-nosnippet class="join">
                <div data-nosnippet class="container">
                    <div data-nosnippet class="join-wrapper">
                        <div data-nosnippet class="join-text">
                            <p><span data-nosnippet>Nantikan Keseruan Festival Musik</span></p>
                            <div data-nosnippet class="join-text-headline">
                                <div data-nosnippet>
                                    <h4><span data-nosnippet>SEGERA</span></h4>
                                    <h4><span data-nosnippet>SEGERA</span></h4>
                                </div>
                                <div data-nosnippet>
                                    <h4><span data-nosnippet>DI TANGGAL</span></h4>
                                    <h4><span data-nosnippet>DI TANGGAL</span></h4>
                                </div>
                                <div data-nosnippet>
                                    <h4><span data-nosnippet>15 OKTOBER 2021</span></h4>
                                    <h4><span data-nosnippet>15 OKTOBER 2021</span></h4>
                                </div>
                            </div>
                        </div>
                        <div data-nosnippet class="join-bg">
                            <div data-nosnippet class="inner">
                                <div data-nosnippet class="join-img">
                                    <div data-nosnippet class="inner" style="background-image: url(https://images.unsplash.com/photo-1459749411175-04bf5292ceea?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80);"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer>
                <div data-nosnippet class="container">
                    <p><span data-nosnippet>Website ini ditujukan untuk perokok dewasa berusia 18+ dan berdomisili di Indonesia.</span></p>
                </div>
            </footer>
        </main>
        <a href="https://rctiplus.com/" onclick="tracker('special_event_interaction', 'click_join_festival', 'button_join_here');" class="nontondisini">
            <div data-nosnippet class="nontondisini__bg">
                <h5>
                    <span data-nosnippet>GO TO HOME</span>
                </h5>
            </div>
        </a>
        <div data-nosnippet class="ghw"><div data-nosnippet class="ghw__wrapper"><div data-nosnippet class="ghw__icon"><img alt="GHW" src="{{$staticAssetPrefix}}/static/unbranded/assets/images/ghw_icon.png"></div><div data-nosnippet class="ghw__warning"><span data-nosnippet><strong>PERHATIAN: </strong></span>KARENA MEROKOK, SAYA TERKENA KANKER TENGGOROKAN. LAYANAN BERHENTI MEROKOK (0800-177-6565)</div><div data-nosnippet class="ghw__icon18">18+</div></div></div>


        <!-- Cookie consent -->
        <script>
            function closeCookieConsent() {
                document.getElementById("cookie_consent").style.display = "none"
            }
        </script>
        <div data-nosnippet id="cookie_consent">
            <div data-nosnippet class="cookie_wrapper">
                <div data-nosnippet class="cookie_child">
                    <img
                        src="{{$staticAssetPrefix}}/static/unbranded/assets/images/cookie_consent.png"
                        alt="cookie consent"
                        width="auto"
                        height="auto" />
                    <p><span data-nosnippet>This site use cookies to optimise site functionality and give you the best possible experience.</span></p>
                </div>
                <div data-nosnippet class="cookie_child">
                    <button
                        onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-out');setDwUserConsent('refuse');"
                        class="cookie_btn unconfirmed">
                        REFUSE
                    </button>
                    <button
                        onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-in');setDwUserConsent('accept');"
                        class="cookie_btn confirmed">
                        ACCEPT
                    </button>
                </div>
                <img
                    role="button"
                    onclick="closeCookieConsent()"
                    class="close_cookie"
                    src="{{$staticAssetPrefix}}/static/unbranded/assets/images/close_cookie.png"
                    alt="close cookie"
                    width="10"
                    height="10"  />
            </div>
        </div>
        <script>
            if (checkDwCookie("dw_user_consent") !== "") {
                document.getElementById("cookie_consent").style.display = "none";
            }
        </script>


        <script src="{{$staticAssetPrefix}}/static/unbranded/js/main.min-min.js?v={{$assetVersion}}"></script>
    </body>
</html>
