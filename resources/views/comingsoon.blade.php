<!DOCTYPE html>
<html lang="en">
    <head>
        <script>let pageActiveTab = 'soundrenaline';</script>

        <!-- Global site tag (gtag.js) - Google Analytics GA Classic Pillar Video -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$gaClassicVideoId}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$gaClassicVideoId}}');
        </script>


        <!-- Global site tag (gtag.js) - Google Analytics GA 4 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$ga4Id}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$ga4Id}}');
        </script>


        <!-- Google Tag Manager -->
        <script>
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'pillar' : 'video'
            });
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth={{$gtmAuth}}'+
                '&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','{{$gtmId}}');
        </script>


        <!-- Google Tag Manager - DMP -->
        <script>
            window.dataLayerDMP = window.dataLayerDMP || [];
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore( j,f);
            })(window,document,'script','dataLayerDMP','{{$gtmDmpId}}');
        </script>


        <!-- comScore -->
        <script>
            let _comscore = [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
                let s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>


        <!-- Alexa Certify -->
        <script type="text/javascript">
            _atrk_opts = { atrk_acct:"8oNJt1FYxz20cv", domain:"rctiplus.com",dynamic: true};
            (function() { let as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; let s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>


        <!-- Tracker function -->
        <script>
            function tracker (event_category, event_action, event_label) {
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                });
            }
            window.tracker = tracker;
            function trackerBanner(event_category, event_action, event_label, id, name, type) {
                let video_category = 'not_available';
                let video_content_type = 'photo';
                if (type === 'video') {
                    video_category = 'vod';
                    video_content_type = 'clip';
                }
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                    'banner_id': id,
                    'banner_name': name,
                    'content_id': 'not_available',
                    'content_name': 'not_available',
                    'video_channel_id': 'not_available',
                    'program_id': 'not_available',
                    'program_name': 'not_available',
                    'video_category': video_category,
                    'video_content_type': video_content_type,
                });
            }
            window.trackerBanner = trackerBanner;
            function trackerPlayer(event_category, event_action, event_label, id, title, name, status, date) {
                let video_content_type = 'live event';
                if (status !== 'live') {
                    video_content_type = 'missed event';
                }
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                    'content_id': id,
                    'content_name': title,
                    'video_channel_id': id,
                    'program_id': 'not_available',
                    'program_name': 'not_available',
                    'video_category': status,
                    'video_content_type': video_content_type,
                    'cast': name,
                    'event_schedule_time': date,
                });
            }
            window.trackerPlayer = trackerPlayer;
        </script>


        <!-- Tracker function - DMP -->
        <script>
            function trackerDMPCookie (cookieValue = 'opt-out') {
                window.dataLayerDMP.push({
                    'event': 'cookieSettings',
                    'cookieType': 'advertising',
                    'cookieValue': cookieValue
                });
            }
            function checkDwCookie(name) {
                let ca = document.cookie.split(';');
                for(let i = 0; i < ca.length; i++) {
                    let c = ca[i];
                    while (c.charAt(0) === ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) === 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }
            function setDwAdvertisementCookie(triggerTracker = false, value = 'opt-out', exdays = 365, init = false) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                if (checkDwCookie("dw_Advertisement_cookie") === "") {
                    document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                    if (triggerTracker === true) {
                        trackerDMPCookie(value);
                    }
                } else {
                    if (init === false) {
                        document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                        if (triggerTracker === true) {
                            trackerDMPCookie(value);
                        }
                    }
                }
            }
            function setDwUserConsent(value = 'false', exdays = 365) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                document.cookie = 'dw_user_consent=' + value + ";" + expires + ";path=/soundrenaline";
            }
            setDwAdvertisementCookie(false, 'opt-out', 365, true);
            window.trackerDMPCookie = trackerDMPCookie;
        </script>


        <!-- Meta tags -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{$staticAssetPrefix}}/static/css/index.css?v={{$assetVersion}}" rel="stylesheet" />


        <?php
        $metaTitle = "Creators Park | Soundrenaline";
        $metaDesc = null;
        $metaKeywords = null;
        $metaImage = null;
        $metaImageAlt = null;
        if ($meta) {
            $metaTitle = $meta->meta_title;
            $metaDesc = $meta->meta_description;
            $metaKeywords = $meta->meta_keywords;
            $metaImage = $meta->meta_image_url;
            $metaImageAlt = $meta->meta_image_alt;
        }
        ?>


        <!-- SEO meta tags -->
        <title>{{$metaTitle}}</title>
        <link rel="icon" type="image/png" href="/assets/image/elements/favicon.png">
        <meta name="title" content="{{$metaTitle}}">
        <meta name="description" content="{{$metaDesc}}">
        <meta name="keywords" content="{{$metaKeywords}}">
        <meta property="og:title" content="{{$metaTitle}}">
        <meta property="og:description" content="{{$metaDesc}}">
        <meta property="og:type" content="website">
        <meta property="og:url" content="{{$pageUrl}}">
        <meta property="og:image" content="{{$metaImage}}">
        <meta property="fb:app_id" content="211272363627736">
        <meta property="og:site_name" content="RCTI+">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@RCTIPlus">
        <meta name="twitter:creator" content="@RCTIPlus">
        <meta name="twitter:title" content="{{$metaTitle}}">
        <meta name="twitter:description" content="{{$metaDesc}}">
        <meta name="twitter:image" content="{{$metaImage}}">
        <meta name="twitter:image:alt" content="{{$metaImageAlt}}">
        <meta name="twitter:url" content="{{$pageUrl}}">
        <meta name="twitter:domain" content="{{$pageUrl}}">


        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/css/datepicker.min.css">
        <script src="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/js/datepicker.min.js"></script>
        <style>
            @font-face {
                font-family: gopher;
                src: url({{$staticAssetPrefix}}/static/fonts/Gopher-Heavy.otf);
            }

            @font-face {
                font-family: saphiro-nrm;
                src: url({{$staticAssetPrefix}}/static/fonts/Shapiro\ 35\ Feather.otf);
            }

            @font-face {
                font-family: saphiro-hvy;
                src: url({{$staticAssetPrefix}}/static/fonts/Shapiro\ 75\ Heavy\ Text.otf);
            }

            main {
                background-image: url({{$staticAssetPrefix}}/static/images/bg_pattern_desktop.webp);
                width: 100vw;
                height: 100vh;
                background-size: cover;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            @media screen and (max-width: 768px) {
                main {
                    background-image: url({{$staticAssetPrefix}}/static/images/bg_pattern_mobile.webp);
                    height: calc(100vh - 7rem);
                }
            }
        </style>
    </head>
    <body>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmId}}&gtm_auth={{$gtmAuth}}&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmDmpId}}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1">
        </noscript>
        <noscript>
            <img src="https://certify.alexametrics.com/atrk.gif?account=8oNJt1FYxz20cv" style="display:none" height="1" width="1" alt="" />
        </noscript>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script type="application/javascript" src="{{$staticAssetPrefix}}/static/javascript/index.min.js?v={{$assetVersion}}"></script>


        <!-- Age verification -->
        @if (!$verifyAgeSession)
            <section class="modal_privy">
                <section class="modal_wrapper">
                    <header>
                        <img
                            class="privy_md_desktop"
                            src="{{$staticAssetPrefix}}/static/images/privy_banner_md_desktop.png"
                            alt="privy image"
                            width="500"
                            height="200" />
                        <p>Situs ini memiliki informasi mengenai produk yang mengandung tembakau dan hanya diperuntukkan bagi perokok berusia 18+ yang berdomisili di Indonesia.</p>
                    </header>
                    <main>
                        <h3>MOHON ISI DATA DIBAWAH INI</h3>
                        <span class="error_msg" style="display: block;"></span>
                        <form id="register_privy">
                            <div class="input_wrapper">
                                <label>Nama lengkap sesuai KTP</label>
                                <div class="input_field">
                                    <input
                                        id="fullname_field"
                                        required
                                        type="text"
                                        placeholder="Masukkan nama"
                                    />
                                </div>
                            </div>
                            <div class="input_wrapper">
                                <label>Nomor KTP</label>
                                <div class="input_field">
                                    <input
                                        id="idcardno_field"
                                        required
                                        type="number"
                                        maxlength="16"
                                        onkeydown="validateNumber(this)"
                                        placeholder="Contoh 16293001289841090"
                                    />
                                </div>
                            </div>
                            <div class="input_wrapper">
                                <label>Tanggal Lahir Sesuai KTP</label>
                                <div class="input_field">
                                    <input
                                        id="birthdate_field"
                                        placeholder="Masukkan tanggal lahir"
                                        inputmode="none"
                                        autocomplete="off"
                                        required />
                                </div>
                            </div>
                            <button class="link_btn">SUBMIT</button>
                        </form>
                    </main>
                    <footer>
                        <p>
                            Hubungi Customer Service Kami Melalui <br />
                            Email: <strong class="txt-bold" style="color: #434343;">CS@RCTIPLUS.COM</strong> atau WA number <br>
                            <strong class="txt-bold" style="color: #434343;">08888988880</strong>
                        </p>
                    </footer>
                </section>
            </section>
        @endif


        <header id="header" class="header">
            <div class="mobile_back">
                <a href="{{$previousPage}}">
                    <img
                        src="{{$staticAssetPrefix}}/static/icons/back_btn.svg"
                        role="button"
                        style="cursor: pointer;"
                        alt="back button" />
                </a>
            </div>
        </header>
        <main>
            <section class="coming_soon_wrapper">
                <img
                    src="{{$staticAssetPrefix}}/static/icons/logo_x_white.png"
                    class="grid_content logo"
                    width="553"
                    height="133"
                    alt="soundrenaline 2021" />
                <article class="article grid_content">
                    <h1 class="child">COMING SOON</h1>
                    <h2 class="child">Our Creators Park will be available on 15 October 2021!</h2>
                </article>
                <a
                    id="desktop_back"
                    href="/soundrenaline"
                    class="link_btn grid_content">
                    BACK TO HOME
                </a>
            </section>
        </main>
        <footer>
            <section>


                <!-- Cookie consent -->
                <div id="cookie_consent">
                    <div class="cookie_wrapper">
                        <div class="cookie_child">
                            <img
                                src="{{$staticAssetPrefix}}/static/icons/cookie_consent.png"
                                alt="cookie consent"
                                width="auto"
                                height="auto" />
                            <p>This site use cookies to optimise site functionality and give you the best possible experience.</p>
                        </div>
                        <div class="cookie_child">
                            <button
                                onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-out');setDwUserConsent('refuse');"
                                class="cookie_btn unconfirmed">
                                REFUSE
                            </button>
                            <button
                                onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-in');setDwUserConsent('accept');"
                                class="cookie_btn confirmed">
                                ACCEPT
                            </button>
                        </div>
                        <img
                            role="button"
                            onclick="closeCookieConsent()"
                            class="close_cookie"
                            src="{{$staticAssetPrefix}}/static/icons/close_cookie.png"
                            alt="close cookie"
                            width="10"
                            height="10"  />
                    </div>
                </div>
                <script>
                    if (checkDwCookie("dw_user_consent") !== "") {
                        document.getElementById("cookie_consent").style.display = "none";
                    }
                </script>


                <div id="smoking_warning">
                    <img src="{{$staticAssetPrefix}}/static/icons/thorax.png" width="auto" height="auto" alt="smoking impact" />
                    <p style="font-family: 'Inter', sans-serif;">
                        PERINGATAN: <br />
                        KARENA MEROKOK, SAYA TERKENA KANKER TENGGOROKAN, LAYANAN BERHENTI MEROKOK (0800-177-6565)
                    </p>
                    <img src="{{$staticAssetPrefix}}/static/icons/18plus.png" width="auto" height="auto" alt="18 plus" />
                </div>
            </section>
        </footer>
    </body>
</html>
