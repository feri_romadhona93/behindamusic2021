<!DOCTYPE html>
<html lang="en">
    <head>
        <script>let pageActiveTab = 'schedule';</script>


        <!-- Global site tag (gtag.js) - Google Analytics GA Classic Pillar Video -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$gaClassicVideoId}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$gaClassicVideoId}}');
        </script>


        <!-- Global site tag (gtag.js) - Google Analytics GA 4 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$ga4Id}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$ga4Id}}');
        </script>


        <!-- Google Tag Manager -->
        <script>
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'pillar' : 'video'
            });
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth={{$gtmAuth}}'+
                '&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','{{$gtmId}}');
        </script>


        <!-- Google Tag Manager - DMP -->
        <script>
            window.dataLayerDMP = window.dataLayerDMP || [];
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore( j,f);
            })(window,document,'script','dataLayerDMP','{{$gtmDmpId}}');
        </script>


        <!-- comScore -->
        <script>
            let _comscore = [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
                let s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>


        <!-- Alexa Certify -->
        <script type="text/javascript">
            _atrk_opts = { atrk_acct:"8oNJt1FYxz20cv", domain:"rctiplus.com",dynamic: true};
            (function() { let as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; let s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>


        <!-- Tracker function -->
        <script>
            function tracker (event_category, event_action, event_label) {
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                });
            }
            window.tracker = tracker;
            function trackerBanner(event_category, event_action, event_label, id, name, type) {
                let video_category = 'not_available';
                let video_content_type = 'photo';
                if (type === 'video') {
                    video_category = 'vod';
                    video_content_type = 'clip';
                }
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                    'banner_id': id,
                    'banner_name': name,
                    'content_id': 'not_available',
                    'content_name': 'not_available',
                    'video_channel_id': 'not_available',
                    'program_id': 'not_available',
                    'program_name': 'not_available',
                    'video_category': video_category,
                    'video_content_type': video_content_type,
                });
            }
            window.trackerBanner = trackerBanner;
            function trackerPlayer(event_category, event_action, event_label, id, title, name, status, date) {
                let video_content_type = 'live event';
                if (status !== 'live') {
                    video_content_type = 'missed event';
                }
                window.dataLayer.push({
                    'pillar': 'video',
                    'event': 'general_event',
                    'event_id': 'not_available',
                    'event_name': 'soundrenaline',
                    'event_category': event_category,
                    'event_action': event_action,
                    'event_label': event_label,
                    'user_id': '',
                    'client_id': '{{$gaClassicAllId}}',
                    'content_id': id,
                    'content_name': title,
                    'video_channel_id': id,
                    'program_id': 'not_available',
                    'program_name': 'not_available',
                    'video_category': status,
                    'video_content_type': video_content_type,
                    'cast': name,
                    'event_schedule_time': date,
                });
            }
            window.trackerPlayer = trackerPlayer;
        </script>


        <!-- Tracker function - DMP -->
        <script>
            function trackerDMPCookie (cookieValue = 'opt-out') {
                window.dataLayerDMP.push({
                    'event': 'cookieSettings',
                    'cookieType': 'advertising',
                    'cookieValue': cookieValue
                });
            }
            function checkDwCookie(name) {
                let ca = document.cookie.split(';');
                for(let i = 0; i < ca.length; i++) {
                    let c = ca[i];
                    while (c.charAt(0) === ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) === 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }
            function setDwAdvertisementCookie(triggerTracker = false, value = 'opt-out', exdays = 365, init = false) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                if (checkDwCookie("dw_Advertisement_cookie") === "") {
                    document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                    if (triggerTracker === true) {
                        trackerDMPCookie(value);
                    }
                } else {
                    if (init === false) {
                        document.cookie = 'dw_Advertisement_cookie=' + value + ";" + expires + ";path=/soundrenaline";
                        if (triggerTracker === true) {
                            trackerDMPCookie(value);
                        }
                    }
                }
            }
            function setDwUserConsent(value = 'false', exdays = 365) {
                const d = new Date();
                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                let expires = "expires="+ d.toUTCString();
                document.cookie = 'dw_user_consent=' + value + ";" + expires + ";path=/soundrenaline";
            }
            setDwAdvertisementCookie(false, 'opt-out', 365, true);
            window.trackerDMPCookie = trackerDMPCookie;
        </script>


        <!-- Meta tags -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="{{$staticAssetPrefix}}/static/css/index.css?v={{$assetVersion}}" rel="stylesheet" />


        <?php
        $metaTitle = "Schedule | Soundrenaline";
        $metaDesc = null;
        $metaKeywords = null;
        $metaImage = null;
        $metaImageAlt = null;
        $metaUploadDate = "2021-09-15T08:00:00+07:00";
        $metaDuration = "PT12H";
        if ($meta) {
            $metaTitle = $meta->meta_title;
            $metaDesc = $meta->meta_description;
            $metaKeywords = $meta->meta_keywords;
            $metaImage = $meta->meta_image_url;
            $metaImageAlt = $meta->meta_image_alt;
        }
        ?>


        <!-- SEO meta tags -->
        <title>{{$metaTitle}}</title>
        <link rel="icon" type="image/png" href="/assets/image/elements/favicon.png">
        <meta name="title" content="{{$metaTitle}}">
        <meta name="description" content="{{$metaDesc}}">
        <meta name="keywords" content="{{$metaKeywords}}">
        <meta property="og:title" content="{{$metaTitle}}">
        <meta property="og:description" content="{{$metaDesc}}">
        <meta property="og:type" content="website">
        <meta property="og:url" content="{{$pageUrl}}">
        <meta property="og:image" content="{{$metaImage}}">
        <meta property="fb:app_id" content="211272363627736">
        <meta property="og:site_name" content="RCTI+">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@RCTIPlus">
        <meta name="twitter:creator" content="@RCTIPlus">
        <meta name="twitter:title" content="{{$metaTitle}}">
        <meta name="twitter:description" content="{{$metaDesc}}">
        <meta name="twitter:image" content="{{$metaImage}}">
        <meta name="twitter:image:alt" content="{{$metaImageAlt}}">
        <meta name="twitter:url" content="{{$pageUrl}}">
        <meta name="twitter:domain" content="{{$pageUrl}}">


        <!-- JSON-LD -->
        <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "Organization",
                "name": "RCTI+",
                "alternateName": "RCTI+",
                "url": "{{$pageUrl}}",
                "logo": "{{$metaImage}}",
                "sameAs": [
                    "https://www.facebook.com/RCTIPlusOfficial/",
                    "https://twitter.com/RCTIPlus",
                    "https://www.instagram.com/rctiplusofficial/",
                    "https://www.linkedin.com/company/rctiplus/",
                    "https://id.wikipedia.org/wiki/RCTI%2B",
                    "https://www.rctiplus.com/"
                ]
            }
        </script>


        <!-- Internal style -->
        <style>
            @font-face {
                font-family: gopher;
                src: url({{$staticAssetPrefix}}/static/fonts/Gopher-Heavy.otf);
            }

            @font-face {
                font-family: saphiro-nrm;
                src: url({{$staticAssetPrefix}}/static/fonts/Shapiro\ 35\ Feather.otf);
            }

            @font-face {
                font-family: saphiro-hvy;
                src: url({{$staticAssetPrefix}}/static/fonts/Shapiro\ 75\ Heavy\ Text.otf);
            }

            #app {
                overflow-x: hidden;
                z-index: 10;
                background-image: url({{$staticAssetPrefix}}/static/images/bg_main_desktop.svg);
                background-position: top;
                background-repeat: repeat-y;
                background-size: cover;
                position: relative;
                min-height: calc(100vh - 48px);
            }

            .masked_trapezoid {
                -webkit-mask-image: url({{$staticAssetPrefix}}/static/icons/mask_img.svg);
                -webkit-mask-repeat: no-repeat;
                -webkit-mask-position: center;
                -webkit-mask-size: 9rem;
                mask-image: url({{$staticAssetPrefix}}/static/icons/mask_img.svg);
                mask-repeat: no-repeat;
                mask-position: center;
                mask-size: 9rem;
            }

            .trapezoid.vod  .schedule_icon {
                background: url({{$staticAssetPrefix}}/static/icons/vod.svg);
                background-position: center;
                background-repeat: no-repeat;
                background-size: contain;
                transform: skewX(10deg);
            }
        </style>


        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/css/datepicker.min.css">
        <script src="https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/js/datepicker.min.js"></script>
    </head>
    <body>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmId}}&gtm_auth={{$gtmAuth}}&gtm_preview={{$gtmEnv}}&gtm_cookies_win=x" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={{$gtmDmpId}}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <noscript>
            <img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1">
        </noscript>
        <noscript>
            <img src="https://certify.alexametrics.com/atrk.gif?account=8oNJt1FYxz20cv" style="display:none" height="1" width="1" alt="" />
        </noscript>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script type="application/javascript" src="{{$staticAssetPrefix}}/static/javascript/index.min.js?v={{$assetVersion}}"></script>


        <!-- Loader -->
        <div id="loader">
            <img src="{{$staticAssetPrefix}}/static/icons/rctiplus.png" width="auto" height="auto" alt="soundrenaline 2021" />
            <div>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>


        <!-- Age verification -->
        @if (!$verifyAgeSession)
            <section class="modal_privy">
                <section class="modal_wrapper">
                    <header>
                        <img
                            class="privy_md_desktop"
                            src="{{$staticAssetPrefix}}/static/images/privy_banner_md_desktop.png"
                            alt="privy image"
                            width="500"
                            height="200" />
                        <p>Situs ini memiliki informasi mengenai produk yang mengandung tembakau dan hanya diperuntukkan bagi perokok berusia 18+ yang berdomisili di Indonesia.</p>
                    </header>
                    <main>
                        <h3>MOHON ISI DATA DIBAWAH INI</h3>
                        <span class="error_msg" style="display: block;"></span>
                        <form id="register_privy">
                            <div class="input_wrapper">
                                <label>Nama lengkap sesuai KTP</label>
                                <div class="input_field">
                                    <input
                                        id="fullname_field"
                                        required
                                        type="text"
                                        placeholder="Masukkan nama"
                                    />
                                </div>
                            </div>
                            <div class="input_wrapper">
                                <label>Nomor KTP</label>
                                <div class="input_field">
                                    <input
                                        id="idcardno_field"
                                        required
                                        type="number"
                                        maxlength="16"
                                        onkeydown="validateNumber(this)"
                                        placeholder="Contoh 16293001289841090"
                                    />
                                </div>
                            </div>
                            <div class="input_wrapper">
                                <label>Tanggal Lahir Sesuai KTP</label>
                                <div class="input_field">
                                    <input
                                        id="birthdate_field"
                                        placeholder="Masukkan tanggal lahir"
                                        inputmode="none"
                                        autocomplete="off"
                                        required />
                                </div>
                            </div>
                            <button class="link_btn">SUBMIT</button>
                        </form>
                    </main>
                    <footer>
                        <p>
                            Hubungi Customer Service Kami Melalui <br />
                            Email: <strong class="txt-bold" style="color: #434343;">CS@RCTIPLUS.COM</strong> atau WA number <br>
                            <strong class="txt-bold" style="color: #434343;">08888988880</strong>
                        </p>
                    </footer>
                </section>
            </section>
        @endif


        <main id="app">


            <!-- Navbar header -->
            <header id="header_content">
                <div id="header" class="header">
                    <figure class="nav_logo">
                        <h1>
                            <img
                                class="collaboration_logo"
                                src="{{$staticAssetPrefix}}/static/icons/logo_x_white.png"
                                width="150"
                                height="auto"
                                alt="soundrenaline 2021" />
                        </h1>
                    </figure>
                    <div class="mobile_back">
                        <a href="{{$previousPage}}">
                            <img
                                src="{{$staticAssetPrefix}}/static/icons/back_btn.svg"
                                role="button"
                                style="cursor: pointer;"
                                alt="back button" />
                        </a>
                        <h1>
                            <img
                                class="collaboration_logo"
                                src="{{$staticAssetPrefix}}/static/icons/logo_x_black.png"
                                width="100rem"
                                height="auto"
                                alt="soundrenaline 2021" />
                        </h1>
                    </div>
                    <nav>
                        <ul class="nav">
                            <li>
                                <a
                                    title="index"
                                    href="/soundrenaline"
                                    onclick="tracker('special_event_interaction', 'click_top_menu', 'home');"
                                >
                                    Home
                                </a>
                                <span></span>
                            </li>
                            <li>
                                <a
                                    title="performance"
                                    href="/soundrenaline/performance"
                                    onclick="tracker('special_event_interaction', 'click_top_menu', 'performance');"
                                >
                                    Performance
                                </a>
                                <span></span>
                            </li>
                            <li>
                                <a
                                    title="schedule"
                                    href="/soundrenaline/schedule"
                                    onclick="tracker('special_event_interaction', 'click_top_menu', 'schedule');"
                                >
                                    Schedule
                                </a>
                                <span></span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>


            <section class="panels">
                <header>
                    <h2 class="section_title text_xl">SCHEDULE</h2>
                </header>
                <main id="schedule">
                    <section class="weekly_nav">
                        <ul>
                            @if($allWeeksFromPerformances)
                                @foreach($allWeeksFromPerformances as $key => $val)
                                    <?php
                                    $id = $key + 1;
                                    $week = $val;
                                    $weekNo = explode('_', $week)[1];
                                    $weekText = 'WEEK ' . $weekNo;
                                    ?>
                                    <li class="txt-bold" onclick="tracker('special_event_interaction', 'click_view_schedule', '{{strtolower($weekText)}}');toggleSchedule('{{$id}}')">
                                        {{$weekText}}
                                        <span></span>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </section>
                    <?php
                    if ($currentDate) {
                        $currentDateId = date("Ymd", strtotime($currentDate));
                    } else {
                        $currentDateId = date('Ymd');
                    }
                    ?>
                    <section id="schedule_wrapper" name="schedule-{{$currentDateId}}" class="schedule_wrapper">
                        @if ($allWeeksPerformancesDateGroup)
                            @foreach($allWeeksPerformancesDateGroup as $key => $val)
                                <?php
                                $startDate = explode(' ', $val['start_date'])[0];
                                $startDate = date("Ymd", strtotime($startDate));
                                $expiredDate = explode(' ', $val['expired_date'])[0];
                                $expiredDate = date("Ymd", strtotime($expiredDate));
                                $idDate = $startDate.'-'.$expiredDate;
                                $performances = $val['performances'];
                                ?>
                                <section id="{{$idDate}}" class="weekly_schedule">
                                    @if ($performances)
                                        <?php $odd = True; ?>
                                        @foreach($performances as $v)
                                            <?php
                                            if ($odd) {
                                                $odd = False;
                                            } else {
                                                $odd = True;
                                            }
                                            $id = $v->id;
                                            $titleSlug = $v->title_slug;
                                            $performanceStatusNow = $v->performance_status_now;
                                            $type = $v->type;
                                            $thumbnailUrl = $v->thumbnail_url;
                                            if (!$thumbnailUrl) {
                                                $thumbnailUrl = $staticAssetPrefix. '/static/icons/placeholder.svg';
                                            }
                                            $performerName = strtoupper($v->performer_name);
                                            $startLiveTime = explode(':', explode(' ', $v->start_live)[1]);
                                            $startLiveTime = $startLiveTime[0] . '.' . $startLiveTime[1];
                                            $endLiveTime = explode(':', explode(' ', $v->end_live)[1]);
                                            $endLiveTime = $endLiveTime[0] . '.' . $endLiveTime[1];
                                            $startLiveDate = explode(' ', $v->start_live)[0];
                                            $startLiveDate = date("j F Y", strtotime($startLiveDate));
                                            //$liveDate = strtoupper($startLiveTime . ' - ' . $endLiveTime . ' / ' . $startLiveDate);
                                            $title = $v->title;
                                            $dateTracker = explode(' ', $v->start_live)[0];
                                            $dateTracker = date("l / d F Y", strtotime($dateTracker));

                                            // revision
                                            $sDateSplit = explode('-', explode(' ', $v->start_live)[0]);
                                            $eDateSplit = explode('-', explode(' ', $v->expired_date)[0]);
                                            if ($sDateSplit[1] === $eDateSplit[1]) {
                                                if ($sDateSplit[2] === $eDateSplit[2]) {
                                                    $liveDate = date("j F Y", strtotime(explode(' ', $v->start_live)[0]));
                                                } else {
                                                    $liveDate = date("j", strtotime(explode(' ', $v->start_live)[0])).' - '.date("j F Y", strtotime(explode(' ', $v->expired_date)[0]));
                                                }
                                            } else {
                                                $liveDate = date("j F Y", strtotime(explode(' ', $v->start_live)[0])).' - '.date("j F Y", strtotime(explode(' ', $v->expired_date)[0]));
                                            }
                                            $liveDate = strtoupper($liveDate);
                                            ?>
                                            @switch($performanceStatusNow)
                                                @case('live')
                                                @case('vod')
                                                <div
                                                    class="list_schedule">
                                                    <div
                                                        class="masked_trapezoid"
                                                        style="background: url({{$thumbnailUrl}});
                                                            background-position: center;
                                                            background-repeat: no-repeat;
                                                            background-size: cover;"
                                                    ></div>
                                                    <a
                                                        href="/soundrenaline/performance/{{$id}}/{{$titleSlug}}"
                                                        onclick="trackerPlayer('special_event_interaction', 'click_content', '{{$performerName}}', '{{$id}}', '{{$title}}', '{{$performerName}}', '{{$performanceStatusNow}}', '{{$dateTracker}}');"
                                                        class="trapezoid vod"
                                                    >
                                                        <div class="list_text">
                                                            <h3>{{$performerName}}</h3>
                                                            <p>{{$liveDate}}</p>
                                                        </div>
                                                        <figure class="schedule_icon"></figure>
                                                    </a>
                                                </div>
                                                @break
                                                @default
                                                <div
                                                    class="list_schedule">
                                                    <div
                                                        class="masked_trapezoid"
                                                        style="background: url({{$thumbnailUrl}});
                                                            background-position: center;
                                                            background-repeat: no-repeat;
                                                            background-size: cover;"
                                                    ></div>
                                                    <div class="trapezoid">
                                                        <div class="list_text">
                                                            <h3>{{$performerName}}</h3>
                                                            <p>{{$liveDate}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @break
                                            @endswitch
                                        @endforeach
                                    @endif
                                </section>
                            @endforeach
                        @endif
                    </section>
                </main>
            </section>


            <section class="panels">
                <article class="creators_park_banner">
                    <p class="creators_park_text">
                        GET THE FULL SOUNDRENALINE EXPERIENCE ONLY AT CREATORS PARK
                    </p>
                    <a
                        href="/soundrenaline/creators-park"
                        onclick="tracker('special_event_interaction', 'click_redirect', 'redirect_to_creators_park');"
                        class="link_btn"
                    >
                        @if ($isLive)
                            <h2 class="text_lg">
                                GO TO CREATORS PARK
                            </h2>
                        @else
                            <h2 class="text_lg">
                                CHECK MORE DETAILS
                            </h2>
                        @endif
                    </a>
                </article>
            </section>
        </main>
        <figure
            class="pattern_img_desktop"
            style="position: absolute;
                right: 0;
                top: 0;
                background-image: url({{$staticAssetPrefix}}/static/images/rt_pattern_desktop.svg);
                background-position: top;
                background-size: cover;
                background-repeat: no-repeat;
                width: 275px;
                height: 200px;">
        </figure>
        <figure
            class="pattern_img_desktop"
            style="position: absolute;
                right: 0;
                bottom: 1rem;
                background-image: url({{$staticAssetPrefix}}/static/images/rb_pattern_desktop.svg);
                background-position: top;
                background-size: cover;
                background-repeat: no-repeat;
                width: 275px;
                height: 200px;">
        </figure>
        <figure
            class="pattern_img_desktop"
            style="position: absolute;
                left: 0;
                bottom: 0;
                background-image: url({{$staticAssetPrefix}}/static/images/lb_pattern_desktop.svg);
                background-position: top;
                background-size: cover;
                background-repeat: no-repeat;
                width: 600px;
                height: 400px;">
        </figure>
        <footer>
            <section>


                <!-- Cookie consent -->
                <div id="cookie_consent">
                    <div class="cookie_wrapper">
                        <div class="cookie_child">
                            <img
                                src="{{$staticAssetPrefix}}/static/icons/cookie_consent.png"
                                alt="cookie consent"
                                width="auto"
                                height="auto" />
                            <p>This site use cookies to optimise site functionality and give you the best possible experience.</p>
                        </div>
                        <div class="cookie_child">
                            <button
                                onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-out');setDwUserConsent('refuse');"
                                class="cookie_btn unconfirmed">
                                REFUSE
                            </button>
                            <button
                                onclick="closeCookieConsent();setDwAdvertisementCookie(true, 'opt-in');setDwUserConsent('accept');"
                                class="cookie_btn confirmed">
                                ACCEPT
                            </button>
                        </div>
                        <img
                            role="button"
                            onclick="closeCookieConsent()"
                            class="close_cookie"
                            src="{{$staticAssetPrefix}}/static/icons/close_cookie.png"
                            alt="close cookie"
                            width="10"
                            height="10"  />
                    </div>
                </div>
                <script>
                    if (checkDwCookie("dw_user_consent") !== "") {
                        document.getElementById("cookie_consent").style.display = "none";
                    }
                </script>


                <div id="smoking_warning">
                    <img src="{{$staticAssetPrefix}}/static/icons/thorax.png" width="auto" height="auto" alt="smoking impact" />
                    <p style="font-family: 'Inter', sans-serif;">
                        PERINGATAN: <br />
                        KARENA MEROKOK, SAYA TERKENA KANKER TENGGOROKAN, LAYANAN BERHENTI MEROKOK (0800-177-6565)
                    </p>
                    <img src="{{$staticAssetPrefix}}/static/icons/18plus.png" width="auto" height="auto" alt="18 plus" />
                </div>
            </section>
        </footer>
    </body>
</html>
