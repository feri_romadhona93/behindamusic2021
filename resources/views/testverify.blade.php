<!DOCTYPE html>
<html>
<head>
    <script   src="https://code.jquery.com/jquery-3.6.0.min.js"   integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="   crossorigin="anonymous"></script>
</head>
<body>
<p>Register & check your identity to access Soundrenaline page</p>
<form id="register-form">
    <input style="display:none" type="text" id="fnik" name="nik" value="1101011001900001">
    <input style="display:none" type="text" id="fname" name="name" value="INDAH RATNA FURI">
    <input style="display:none" type="text" id="fdob" name="dob" value="10-01-1990"><br>
    <input type="submit" value="Cheat langsung masuk">
</form>
</body>
<script>
    $("#register-form").submit(function(e) {
        e.preventDefault();
        let formData = $("#register-form").serializeArray();
        let data = {};
        formData.map(function(val){
            data[val.name] = val.value;
        });
        data = JSON.stringify(data);
        let url = "{{$submitUrl}}";
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function(res){
                if (res.code == '00') {
                    window.location = "{{$redirectURLIfAllowed}}"
                }
            },
            error: function(res){

            },
            dataType: "json",
            contentType : "application/json"
        });
    });
</script>
</html>
