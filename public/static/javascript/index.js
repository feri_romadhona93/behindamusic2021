let states = {}

function constants() {
    return {
        videoState: {
            ON_PLAY: "play",
            ON_PAUSE: "pause",
            ON_END: "end"
        }
    }
}

function goBack() {
    history.back()
}

function redirect(target, endpoint) {
    // Edited by Dali
    let url = endpoint || target

    const navLinks = document.querySelectorAll("nav > ul li a")
    Array.from(navLinks).forEach(navLink => {
        navLink.classList.remove("active")
    })

    sessionStorage.removeItem("video_url")

    // Edited by Dali
    switch (target) {
        case "index":
            return location.assign(`${url}`)
        case "soundrenaline":
            return location.assign(`${url}`)
        default:
            return location.assign(`${url}`)
    }
}

function getListOfMonths() {
    return [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ]
}

function appendOptions(list, selectionContainer, selectedDate = "") {
    list.forEach(val => {
        const option = document.createElement("option")
        option.setAttribute("value", val)
        option.innerText = val
        selectionContainer.append(option)
    })
}

function onChangeMonth(element) {
    const indexOfSelectedMonth = getListOfMonths().indexOf(element.value)
    const selectedYear = +(document.getElementById("year_selection").value || new Date().getFullYear())
    if (indexOfSelectedMonth < 0) return

    const dateSelection = document.getElementById("date_selection")
    const lastDayOfMonth = new Date(selectedYear, indexOfSelectedMonth + 1, 0).getDate()
    const listOfDays = Array.from(Array(lastDayOfMonth).keys()).map(date => date + 1)

    let previousDateValue = dateSelection.value || "1"
    if (+previousDateValue > lastDayOfMonth) previousDateValue = String(lastDayOfMonth)
    dateSelection.innerHTML = "<option value disabled> Tanggal </option>"

    appendOptions(listOfDays, dateSelection)
    dateSelection.value = previousDateValue
}

function formInvalid(e) {
    e.preventDefault()
    e.target.parentNode.classList.add("invalid")
}

function formInFocus(e) {
    e.target.parentNode.classList.remove("invalid")
    e.target.parentNode.classList.add("focus")
}

function formOutFocus(e) {
    e.target.parentNode.classList.remove("focus")
}

function validateNumber(element) {
    const pattern = /[0-9]{16}/ig
    if (element.value > 16) element.value = element.value.substring(0, 16)

    const valid = pattern.test(element.value) && element.value.length === 16

    valid
        ? element.parentNode.classList.remove("invalid")
        : element.parentNode.classList.add("invalid")
}

function toggleLoader(state) {
    switch (state) {
        case "complete": {
            document.getElementById("loader").style.display = "none"
        }
            return

        default: {
            const [ _, div ] = document.getElementById("loader").children
            Array.from(div.children).forEach((span, i) => {
                setTimeout(function timeout() {
                    span.classList.add("growing")
                    clearTimeout(timeout)
                }, (300 * i))
            })
        }
    }
}

function carouselTouchStart(e) {
    states.touchX = e.touches[0].clientX
}

function carouselTouchEnd(e) {
    if (states.videoState === constants().videoState.ON_PLAY) return

    e.target.style.transform = "unset"
    const isNext = e.changedTouches[0].clientX < states.touchX
    const moreThanOffset = Math.abs(e.changedTouches[0].clientX - states.touchX) > 100

    if (moreThanOffset) changeCarouselContent(isNext)
}

function carouselTouchMove(e) {
    if (states.videoState === constants().videoState.ON_PLAY) return
    e.target.style.transform = `translateX(${-(states.touchX - e.changedTouches[0].clientX)}px)`
}

function initCarousel() {
    const carouselBanner = document.getElementById("carousel_banner")
    if (!carouselBanner) return

    const carouselItemIndicator = document.getElementById("carousel_item_indicator")
    const carouselContent = carouselBanner.querySelectorAll(".carousel_content")
    carouselContent[0].classList.add("active")

    Array.from(carouselContent).forEach((content, i) => {
        content.addEventListener("touchstart", carouselTouchStart)
        content.addEventListener("touchend", carouselTouchEnd)
        content.addEventListener("touchmove", carouselTouchMove)

        const span = document.createElement("span")
        span.addEventListener("click", () => navigateCarousel(i))
        span.setAttribute("class", "indicators")
        span.setAttribute("id", `carousel_indicator_${i}`)
        if (i === 0) span.classList.add("active")

        carouselItemIndicator.appendChild(span)
    })
}

function navigateCarousel(index) {
    const carouselContents = document.getElementById("carousel_banner").querySelectorAll(".carousel_content")
    const indicators = document.getElementById("carousel_item_indicator").children

    Array.from(carouselContents).forEach((content, i) => {
        content.classList.remove("active")
        indicators[i].classList.remove("active")
    })

    carouselContents[index].classList.add("active")
    indicators[index].classList.add("active")
}

function changeCarouselContent(isNext) {
    const carouselContents = document.getElementById("carousel_banner").querySelectorAll(".carousel_content")

    let index = 0
    Array.from(carouselContents).forEach((content, i) => {
        if (content.className.includes("active")) {
            const nextIndex = i + 1 > carouselContents.length - 1 ? 0 : i + 1
            const prevIndex = i - 1 < 0 ? carouselContents.length - 1 : i - 1
            index = isNext ? nextIndex : prevIndex
        }
    })
    navigateCarousel(index)
}

function carouselBtn(direction) {
    const isNext = direction === "next"
    changeCarouselContent(isNext)
}

function setPerformanceSchedule() {
    if (!document.getElementById("schedule")) return

    const scheduleWrapper = document.getElementById("schedule_wrapper")
    const listNav = document.querySelector("#schedule .weekly_nav ul")
    const listSchedule = document.querySelector("#schedule .schedule_wrapper")
    const parameterDate = scheduleWrapper.getAttribute("name").split("-").reverse()[0]

    const selectedYear = parameterDate.split("").slice(0, 4).join("")
    const selectedMonth = parameterDate.split("").slice(4, 6).join("")
    const selectedDate = parameterDate.split("").slice(6, parameterDate.length).join("")

    const currentDate = new Date(`${selectedYear} ${selectedMonth} ${selectedDate}`)

    let activeWeek = 0

    Array.from(listSchedule.children).forEach((listitem, i) => {
        const [ begin, end ] = Array.from(listitem.id.split("-")).map(dateString => {
            const dateComponents = dateString.split("")

            const year = dateComponents.slice(0, 4).join("")
            const month = dateComponents.slice(4, 6).join("")
            const day = dateComponents.slice(6, dateComponents.length).join("")

            return new Date(`${year} ${month} ${day}`)
        })

        if (currentDate >= begin && currentDate <= end) activeWeek = i
    })

    listNav.children[activeWeek].classList.add("active")
    listSchedule.children[activeWeek].classList.add("active")
}

function toggleSchedule(week) {
    const listNav = document.querySelector("#schedule .weekly_nav ul")
    const listSchedule = document.querySelector("#schedule .schedule_wrapper")

    Array.from(listNav.children).forEach((child, i) => {
        child.classList.remove("active")
        listSchedule.children[i].classList.remove("active")
    })

    listSchedule.children[+week - 1].classList.add("active")
    listNav.children[+week - 1].classList.add("active")
}

function soundreTrailersEventHandlers(video, state) {
    states.videoState = state
    const carouselIndicator = document.getElementById("carousel_item_indicator")
    const carouselNavBtns = document.getElementById("carousel_banner").querySelectorAll(".carousel_nav")

    let visibility = "unset"
    switch(states.videoState) {
        case constants().videoState.ON_PLAY:
            visibility = "hidden"
            break
        default:
            visibility = "unset"
    }

    Array.from([...Array.from(carouselNavBtns), carouselIndicator])
        .forEach(el => el.style.visibility = visibility)
}

function dateValidator(el) {
    if (!el.value) return
    const year = +el.value.split("-")[0]
    if ( year > 2003 ) el.value = "2003-12-31"
}

function headerOnscrollListener() {
    const header = document.getElementById("header")

    if (scrollY >= 50) {
        header.style.position = "fixed"
        header.style.zIndex = "1000"
        header.style.width = "100%"
        header.style.background = "#000000e0"
        header.style.animation = "header-animation 1s"
    }
    if (scrollY < 50) {
        header.style.position = ""
        header.style.zIndex = ""
        header.style.animation = ""
        header.style.background = ""
    }
}

function datePicker() {
    const datepickerElement = document.getElementById("birthdate_field")

    if (!datepickerElement) return
    new Datepicker(datepickerElement, {
        maxDate: "2003-12-31",
        minDate: "1940-01-01",
        format: "yyyy-mm-dd",
        disableTouchKeyboard: false,
    })

    Array.from([
        { event: "show", overflow: "hidden" },
        { event: "hide", overflow: "" },
    ]).forEach(({ event, overflow }) => {
        datepickerElement.addEventListener(event, _ => document.body.style.overflow = overflow)
    })
}

function closeCookieConsent() {
    document.getElementById("cookie_consent").style.display = "none"
}

document.addEventListener("readystatechange", () => {
    try {
        toggleLoader(document.readyState)
    } catch (e) {
        //
    }
})

window.onload = () => {
    let activetab = pageActiveTab || "index";

    try {
        initCarousel()
        headerOnscrollListener()
    } catch (e) {
        //
    }

    document.addEventListener("scroll", _ => headerOnscrollListener())

    try {
        datePicker()
    } catch (e) {
        //
    }

    const navLinks = document.querySelectorAll("nav > ul li a")
    Array.from(navLinks).forEach(navLink => {
        if (navLink.title === activetab) navLink.classList.add("active")
    })

    try {
        setPerformanceSchedule()
    } catch (e) {
        //
    }

    // Edited by Dali
    if (document.getElementById('register_privy')) {
        Array.from([
            "fullname_field",
            "idcardno_field",
            "birthdate_field"
        ]).forEach(fieldId => {
            document.getElementById(fieldId).addEventListener("invalid", formInvalid)
            document.getElementById(fieldId).addEventListener("focusin", formInFocus)
            document.getElementById(fieldId).addEventListener("focusout", formOutFocus)
        })
        document.getElementById("register_privy").addEventListener("submit", (event) => {
            event.preventDefault()
            let modalPrivyElem = document.getElementsByClassName('modal_privy')[0]
            let errorMsgElem = document.getElementsByClassName('error_msg')[0]
            errorMsgElem.style.display = 'none'
            let dob = document.getElementById('birthdate_field').value.split('-');
            axios({
                method: "post",
                url: "/soundrenaline/verify-age/submit",
                data: {
                    'name': document.getElementById("fullname_field").value,
                    'nik': document.getElementById("idcardno_field").value,
                    'dob': `${dob[2]}-${dob[1]}-${dob[0]}`
                },
            })
            .then(function (response) {
                let responseData = response.data
                if (responseData.code === '00') {
                    modalPrivyElem.style.display = 'none'
                    window.tracker('special_event_interaction', 'click_submit_credentials', 'successful_submission');
                } else {
                    errorMsgElem.style.display = 'block'
                    errorMsgElem.innerHTML = responseData.message
                    window.tracker('special_event_interaction', 'click_submit_credentials', 'failed_submission');
                }
            })
            .catch(function () {
                errorMsgElem.style.display = 'block'
                errorMsgElem.innerHTML = 'Sedang ada gangguan koneksi, silahkan coba lagi'
            });
        })
    }
}
